<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\Traits\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Providers\HelperServiceProvider;
class Anggota_rombel extends Model
{
    //use Uuid;
	use SoftDeletes;
    public $incrementing = false;
	protected $table = 'anggota_rombel';
	protected $primaryKey = 'anggota_rombel_id';
	protected $guarded = [];
	public function rombongan_belajar(){
		$semester = HelperServiceProvider::get_ta();
		return $this->hasOne('App\Rombongan_belajar', 'rombongan_belajar_id', 'rombongan_belajar_id')->where('jenis_rombel', '=', 1)->where('semester_id', '=', $semester->semester_id);
    }
	public function siswa(){
		return $this->hasOne('App\Siswa', 'siswa_id', 'siswa_id');
	}
	public function nilai_akhir_pengetahuan(){
		return $this->hasOne('App\NilaiAkhirPengetahuan', 'anggota_rombel_id', 'anggota_rombel_id')->where('kompetensi_id', '=', 1);
	}
	public function nilai_akhir_keterampilan(){
		return $this->hasOne('App\NilaiAkhirKeterampilan', 'anggota_rombel_id', 'anggota_rombel_id')->where('kompetensi_id', '=', 2);
	}
	public function nilai_kd_pengetahuan(){
		return $this->hasMany('App\NilaiPengetahuanPerKd', 'anggota_rombel_id', 'anggota_rombel_id')->where('kompetensi_id', '=', 1);
	}
	public function nilai_kd_keterampilan(){
		return $this->hasMany('App\NilaiKeterampilanPerKd', 'anggota_rombel_id', 'anggota_rombel_id')->where('kompetensi_id', '=', 2);
	}
	protected static function boot(){
		parent::boot();
		static::creating(function ($model) {
            try {
                $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
		static::deleting(function($model) {
			//foreach ($model->siswa()->get() as $model) {
				//$model->delete();
			//}
		});
    }
}
