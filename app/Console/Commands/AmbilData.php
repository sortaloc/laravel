<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use App\Providers\HelperServiceProvider;
use Symfony\Component\Console\Output\BufferedOutput;
use App\Kompetensi_dasar;
use Illuminate\Support\Facades\Storage;
class AmbilData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sinkronisasi:ambildata {username_dapo} {password_dapo} {tahun_ajaran_id} {semester_id} {sekolah_id} {npsn} {server} {aksi} {last_sync}';
	//protected $signature = 'sinkronisasi:ambildata {dataName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
		$host_server = $arguments['server'].$arguments['aksi'];
		if($arguments['aksi'] == 'sinkron_sekolah'){
			$host_server = $arguments['server'].'sekolah';
		}
		$response = Curl::to($host_server)->withData($arguments)->post();
		$response = json_decode($response);
		if(isset($response->data)){
			$count = count($response->data);
		} else {
			$count = 0;
		}
		self::{$arguments['aksi']}($response);
    }
	private function status($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['data']	= 'sekolah';
		$result['response'] = $response;
		$result['next'] = 'sekolah';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function sekolah($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'sekolah', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['data'] = 'sekolah';
		$result['aksi'] = 'sinkron_sekolah';
		$result['progress'] = 10;
		$result['server']	= 'erapor_dashboard';
		echo json_encode($result);
	}
	private function sinkron_sekolah($response){
		$array = json_decode(json_encode($response), true);
		$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'sinkron_sekolah', 'data' => $array)]);
		$result['status'] = 1;
		$result['data'] = 'guru';
		$result['aksi'] = 'guru_sync';
		$result['progress'] = 15;
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function guru_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'guru', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['data'] = 'rombongan_belajar';
		$result['aksi'] = 'rombongan_belajar_sync';
		$result['progress'] = 20;
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function rombongan_belajar_sync($response){
		$this->output = new BufferedOutput;
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'rombongan_belajar', 'data' => $array)], $this->output);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 30;
		$result['data'] = 'siswa_aktif';
		$result['aksi'] = 'siswa_sync';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function siswa_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			$count_data = count($response->data);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'siswa_aktif', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
			$count_data = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 40;
		$result['data'] = 'siswa_keluar';
		$result['aksi'] = 'siswa_keluar_sync';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function siswa_keluar_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'siswa_keluar', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 50;
		$result['data'] = 'pembelajaran';
		$result['aksi'] = 'pembelajaran_sync';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function pembelajaran_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'pembelajaran', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 55;
		$result['data'] = 'ekskul';
		$result['aksi'] = 'ekskul_sync';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function ekskul_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'ekskul', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 60;
		$result['data'] = 'anggota_ekskul';
		$result['aksi'] = 'anggota_ekskul_sync';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function anggota_ekskul_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'anggota_ekskul', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 75;
		$result['data'] = 'dudi';
		$result['aksi'] = 'dudi_sync';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function dudi_sync($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'dudi', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 80;
		$result['data'] = 'jurusan';
		$result['aksi'] = 'jurusan';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function jurusan($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'jurusan', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 85;
		$result['data'] = 'kurikulum';
		$result['aksi'] = 'kurikulum';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function kurikulum($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'kurikulum', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 90;
		$result['data'] = 'mata_pelajaran';
		$result['aksi'] = 'mata_pelajaran';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function mata_pelajaran($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'mata_pelajaran', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 95;
		$result['data'] = 'mapel_kur';
		$result['aksi'] = 'mapel_kur';
		$result['server']	= 'erapor_server';
		echo json_encode($result);
	}
	private function mapel_kur($response){
		if(isset($response->post_login)){
			$post_login = $response->post_login;
			$array = json_decode(json_encode($response), true);
			if($post_login){
				$this->call('sinkronisasi:prosesdata',['response' => array('query' => 'mapel_kur', 'data' => $array)]);
			}
		} else {
			$post_login = 0;
		}
		$result['status'] = $post_login;
		$result['progress'] = 97;
		$result['data'] = 'kompetensi_dasar';
		$result['aksi'] = 'count_kd';
		$result['server']	= 'erapor_dashboard';
		echo json_encode($result);
	}
	private function count_kd($response){
		$jumlah_server = $response->ref_kd;
		$jumlah_lokal = Kompetensi_dasar::count();
		$record['table'] = 'referensi kompetensi dasar';
		$record['jumlah'] = $jumlah_server;
		$record['inserted'] = $jumlah_lokal;
		Storage::disk('public')->put('proses_kompetensi_dasar.json', json_encode($record));
		if($jumlah_server > $jumlah_lokal){
			$this->call('kd:start');
		}
		$result['status'] = 0;
	}
}
