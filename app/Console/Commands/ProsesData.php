<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sekolah;
use App\User;
use App\Role;
use App\Role_user;
use App\Providers\HelperServiceProvider;
use App\Mst_wilayah;
use App\Guru;
use App\Gelar_ptk;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Jurusan_sp;
use App\Mata_pelajaran;
use App\Mata_pelajaran_kurikulum;
use Ixudra\Curl\Facades\Curl;
use App\Rombongan_belajar;
use App\Siswa;
Use App\Anggota_rombel;
use App\Ekstrakurikuler;
use App\Dudi;
use App\Mou;
use App\Pembelajaran;
use App\Jurusan;
use App\Kurikulum;
use Illuminate\Support\Facades\Storage;
class ProsesData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sinkronisasi:prosesdata {response*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
		$function = $arguments['response']['query'];
		self::{$function}($arguments['response']['data']);
		//echo $arguments['response']['query'];
		//HelperServiceProvider::test($arguments);
    }
	private function sekolah($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$query = HelperServiceProvider::array_to_object($response);
		$data = $query->kepsek;
		$random = Str::random(6);
		$kecamatan_kepsek = Mst_wilayah::where('kode_wilayah', $data->kode_wilayah)->first();
		$data->email = ($data->email) ? $data->email : strtolower($random).'@erapor-smk.net';
		$data->email = ($data->email != $user->email) ? $data->email : strtolower($random).'@erapor-smk.net';
		$data->email = ($data->email != $sekolah->email) ? $data->email : strtolower($random).'@erapor-smk.net';
		$data->email = strtolower($data->email);
		$data->nuptk = ($data->nuptk) ? $data->nuptk : mt_rand();
		$insert_guru = array(
			'sekolah_id' 			=> $user->sekolah_id,
			'nama' 					=> $data->nama,
			'nuptk' 				=> $data->nuptk,
			'nip' 					=> $data->nip,
			'nik' 					=> $data->nik,
			'jenis_kelamin' 		=> $data->jenis_kelamin,
			'tempat_lahir' 			=> $data->tempat_lahir,
			'tanggal_lahir' 		=> $data->tanggal_lahir,
			'status_kepegawaian_id'	=> $data->status_kepegawaian_id,
			'jenis_ptk_id' 			=> $data->jenis_ptk_id,
			'agama_id' 				=> $data->agama_id,
			'alamat' 				=> $data->alamat_jalan,
			'rt' 					=> $data->rt,
			'rw' 					=> $data->rw,
			'desa_kelurahan' 		=> $data->desa_kelurahan,
			'kecamatan' 			=> $kecamatan_kepsek->nama,
			'kode_pos'				=> ($data->kode_pos) ? $data->kode_pos : 0,
			'no_hp'					=> ($data->no_hp) ? $data->no_hp : 0,
			'email' 				=> $data->email,
			'is_dapodik'			=> 1,
			'last_sync'				=> date('Y-m-d H:i:s'),
		);
		$create_guru = Guru::updateOrCreate(
			['guru_id_dapodik' => $data->ptk_id],
			$insert_guru
		);
		$insert_user = array(
			'name' => $data->nama,
			'email' => $data->email,
			'nuptk'	=> $data->nuptk,
			'password' => Hash::make(12345678),
			'last_sync'	=> date('Y-m-d H:i:s'),
			'sekolah_id'	=> $user->sekolah_id,
			'password_dapo'	=> md5(12345678),
			'guru_id'	=> $create_guru->guru_id
		);
		$create_user = User::updateOrCreate(
			['guru_id' => $create_guru->guru_id],
			$insert_user
		);
		$adminRole = Role::where('name', 'guru')->first();
		$CheckadminRole = DB::table('role_user')->where('user_id', $create_user->user_id)->first();
		if(!$CheckadminRole){
			$create_user->attachRole($adminRole);
		}
		foreach($query->jurusan_sp as $jurusan_sp){
			$insert_jur_sp = array(
				'sekolah_id'	=> $user->sekolah_id,
				'jurusan_id'	=> $jurusan_sp->jurusan_id,
				'nama_jurusan_sp'	=> $jurusan_sp->nama_jurusan_sp,
				'last_sync'	=> date('Y-m-d H:i:s'),
			);
			Jurusan_sp::updateOrCreate(
				['jurusan_sp_id_dapodik' => $jurusan_sp->jurusan_sp_id],
				$insert_jur_sp
			);
		}
		$sekolah->guru_id = $create_guru->guru_id;
		$sekolah->save();
	}
	private function sinkron_sekolah($response){
		$update_sekolah = array('sinkron' => 1);
		$user = auth()->user();
		Sekolah::find($user->sekolah_id)->update($update_sekolah);
	}
	private function guru($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'guru';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_guru.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_guru.json', json_encode($record));
			$data->nuptk = str_replace(' ','',$data->nuptk);
			$data->nuptk = str_replace('-','',$data->nuptk);
			$data->nuptk = str_replace(' ','',$data->nuptk);
			$random = Str::random(6);
			$data->nuptk = ($data->nuptk) ? $data->nuptk : mt_rand();
			$data->email = ($data->email) ? $data->email : strtolower($random).'@erapor-smk.net';
			$data->email = ($data->email != $user->email) ? $data->email : strtolower($random).'@erapor-smk.net';
			$data->email = ($data->email != $sekolah->email) ? $data->email : strtolower($random).'@erapor-smk.net';
			$data->email = strtolower($data->email);
			$kecamatan = Mst_wilayah::where('kode_wilayah', $data->kode_wilayah)->first();
			$insert_guru = array(
				'sekolah_id'			=> $user->sekolah_id,
				'nama' 					=> $data->nama,
				'nuptk' 				=> $data->nuptk,
				'nip' 					=> $data->nip,
				'nik' 					=> $data->nik,
				'jenis_kelamin' 		=> $data->jenis_kelamin,
				'tempat_lahir' 			=> $data->tempat_lahir,
				'tanggal_lahir' 		=> $data->tanggal_lahir,
				'status_kepegawaian_id'	=> $data->status_kepegawaian_id,
				'jenis_ptk_id' 			=> $data->jenis_ptk_id,
				'agama_id' 				=> $data->agama_id,
				'alamat' 				=> $data->alamat_jalan,
				'rt' 					=> ($data->rt) ? $data->rt : 0,
				'rw' 					=> ($data->rw) ? $data->rw : 0,
				'desa_kelurahan' 		=> $data->desa_kelurahan,
				'kecamatan' 			=> $kecamatan->nama,
				'kode_pos'				=> ($data->kode_pos) ? $data->kode_pos : 0,
				'no_hp'					=> ($data->no_hp) ? $data->no_hp : 0,
				'email' 				=> $data->email,
				'last_sync'				=> date('Y-m-d H:i:s'),
			);
			$create_guru = Guru::updateOrCreate(
				['guru_id_dapodik' => $data->ptk_id],
				$insert_guru
			);
			$insert_user = array(
				'name' => $data->nama,
				'email' => $data->email,
				'nuptk'	=> $data->nuptk,
				'password' => Hash::make(12345678),
				'last_sync'	=> date('Y-m-d H:i:s'),
				'sekolah_id'	=> $user->sekolah_id,
				'password_dapo'	=> md5(12345678),
				'guru_id'	=> $create_guru->guru_id
			);
			$create_user = User::updateOrCreate(
				['guru_id' => $create_guru->guru_id],
				$insert_user
			);
			$adminRole = Role::where('name', 'guru')->first();
			$CheckadminRole = DB::table('role_user')->where('user_id', $create_user->user_id)->first();
			if(!$CheckadminRole){
				$create_user->attachRole($adminRole);
			}
			$data_sync = array(
				'ptk_id'	=> $data->ptk_id,
			);
			$host_server = 'http://103.40.55.242/erapor_server/sync/rwy_pend_formal';
			$response = Curl::to($host_server)
			->withData($data_sync)->post();
			$response = json_decode($response);
			$find_gelar = ($response) ? $response->rwy_pend_formal : '';
			if($find_gelar){
				$find_gelar = array_unique($find_gelar, SORT_REGULAR);
				foreach($find_gelar as $gelar){
					if($gelar->gelar_akademik_id){
						$find_gelar_ptk = Gelar_ptk::where([['ptk_id', '=', $data->ptk_id], ['gelar_akademik_id', '=', $gelar->gelar_akademik_id]])->first();
						if($find_gelar_ptk){
							$find_gelar_ptk->delete();
						}
						Gelar_ptk::create(array('gelar_akademik_id' => $gelar->gelar_akademik_id, 'sekolah_id' => $user->sekolah_id, 'ptk_id' => $data->ptk_id, 'guru_id' => $create_guru->guru_id, 'last_sync' => date('Y-m-d H:i:s')));
						}
				}
			}
			$i++;
		}
	}
	private function rombongan_belajar($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'rombongan belajar';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_rombongan_belajar.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_rombongan_belajar.json', json_encode($record));
			$get_jurusan_id = Jurusan_sp::where('jurusan_sp_id_dapodik', '=', $data->jurusan_sp_id)->first();
			$get_wali = Guru::where('guru_id_dapodik', '=', $data->ptk_id)->first();
			$get_user = User::where('guru_id', '=', $get_wali->guru_id)->first();
			$insert_rombel = array(
				'sekolah_id' 			=> $sekolah->sekolah_id,
				'jurusan_id' 			=> $get_jurusan_id->jurusan_id,
				'jurusan_sp_id' 		=> $get_jurusan_id->jurusan_sp_id,
				'kurikulum_id' 			=> $data->kurikulum_id,
				'nama' 					=> $data->nama,
				'guru_id' 				=> $get_wali->guru_id,
				'tingkat' 				=> $data->tingkat_pendidikan_id,
				'guru_id_dapodik' 		=> $data->ptk_id,
				'jenis_rombel'			=> $data->jenis_rombel,
				'last_sync'				=> date('Y-m-d H:i:s'),
			);
			Rombongan_belajar::updateOrCreate(
				['rombel_id_dapodik' => $data->rombongan_belajar_id, 'semester_id' => $semester->semester_id],
				$insert_rombel
			);
			$adminRole = Role::where('name', 'wali')->first();
			$CheckadminRole = DB::table('role_user')->where('user_id', $get_user->user_id)->where('role_id', $adminRole->id)->first();
			if(!$CheckadminRole){
				$get_user->attachRole($adminRole);
			}
			$i++;
		}
	}
	private function siswa_aktif($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'peserta didik aktif';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_siswa_aktif.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_siswa_aktif.json', json_encode($record));
			$data_sync = array(
				'peserta_didik_id'	=> $data->peserta_didik_id,
				'sekolah_id'		=> $sekolah->sekolah_id,
			);
			$host_server = 'http://103.40.55.242/erapor_server/sync/diterima_kelas';
			$response = Curl::to($host_server)
			->withData($data_sync)->post();
			$response = json_decode($response);
			$find_diterima_kelas = isset($response->data->nama) ? $response->data->nama : '-';
			$random = Str::random(6);
			$data->nisn = ($data->nisn) ? $data->nisn : mt_rand();
			$data->email = ($data->email) ? $data->email : strtolower($random).'@erapor-smk.net';
			$data->email = ($data->email != $sekolah->email) ? $data->email : strtolower($random).'@erapor-smk.net';
			$data->email = strtolower($data->email);
			$insert_siswa = array(
				'sekolah_id'		=> $sekolah->sekolah_id,
				'nama' 				=> $data->nama_siswa,
				'no_induk' 			=> ($data->nipd) ? $data->nipd : 0,
				'nisn' 				=> $data->nisn,
				'jenis_kelamin' 	=> ($data->jenis_kelamin) ? $data->jenis_kelamin : 0,
				'tempat_lahir' 		=> ($data->tempat_lahir) ? $data->tempat_lahir : 0,
				'tanggal_lahir' 	=> $data->tanggal_lahir,
				'agama_id' 			=> ($data->agama_id) ? $data->agama_id : 0,
				'status' 			=> 'Anak Kandung',
				'anak_ke' 			=> ($data->anak_keberapa) ? $data->anak_keberapa : 0,
				'alamat' 			=> ($data->alamat_jalan) ? $data->alamat_jalan : 0,
				'rt' 				=> ($data->rt) ? $data->rt : 0,
				'rw' 				=> ($data->rw) ? $data->rw : 0,
				'desa_kelurahan' 	=> ($data->desa_kelurahan) ? $data->desa_kelurahan : 0,
				'kecamatan' 		=> ($data->kecamatan) ? $data->kecamatan : 0,
				'kode_pos' 			=> ($data->kode_pos) ? $data->kode_pos : 0,
				'no_telp' 			=> ($data->nomor_telepon_seluler) ? $data->nomor_telepon_seluler : 0,
				'sekolah_asal' 		=> ($data->sekolah_asal) ? $data->sekolah_asal : 0,
				'diterima_kelas' 	=> ($find_diterima_kelas) ? $find_diterima_kelas : 0,
				'diterima' 			=> ($data->tanggal_masuk_sekolah) ? $data->tanggal_masuk_sekolah : 0,
				'kode_wilayah' 		=> $data->kode_wilayah,
				'email' 			=> $data->email,
				'nama_ayah' 		=> ($data->nama_ayah) ? $data->nama_ayah : 0,
				'nama_ibu' 			=> ($data->nama_ibu_kandung) ? $data->nama_ibu_kandung : 0,
				'kerja_ayah' 		=> ($data->pekerjaan_id_ayah) ? $data->pekerjaan_id_ayah : 1,
				'kerja_ibu' 		=> ($data->pekerjaan_id_ibu) ? $data->pekerjaan_id_ibu : 1,
				'nama_wali' 		=> ($data->nama_wali) ? $data->nama_wali : 0,
				'alamat_wali' 		=> ($data->alamat_jalan) ? $data->alamat_jalan : 0,
				'telp_wali' 		=> ($data->nomor_telepon_seluler) ? $data->nomor_telepon_seluler : 0,
				'kerja_wali' 		=> ($data->pekerjaan_id_wali) ? $data->pekerjaan_id_wali : 1,
				'active' 			=> 1,
				'last_sync'			=> date('Y-m-d H:i:s'),
			);
			$password = 12345678;
			$find_rombel = Rombongan_belajar::where('rombel_id_dapodik', '=', $data->rombongan_belajar_id)->first();
			$create_siswa = Siswa::updateOrCreate(
				['siswa_id_dapodik' => $data->peserta_didik_id],
				$insert_siswa
			);
			$insert_user = array(
				'name' => $data->nama,
				'email' => $data->email,
				'nisn'	=> $data->nisn,
				'password' => Hash::make($password),
				'last_sync'	=> date('Y-m-d H:i:s'),
				'sekolah_id'	=> $sekolah->sekolah_id,
				'password_dapo'	=> md5($password),
				'siswa_id'	=> $create_siswa->siswa_id
			);
			$create_user = User::updateOrCreate(
				['siswa_id' => $create_siswa->siswa_id],
				$insert_user
			);
			$adminRole = Role::where('name', 'siswa')->first();
			$CheckadminRole = DB::table('role_user')->where('user_id', $create_user->user_id)->first();
			if(!$CheckadminRole){
				$create_user->attachRole($adminRole);
			}
			$insert_anggota_rombel = array(
				'sekolah_id'				=> $sekolah->sekolah_id,
				'rombongan_belajar_id' 		=> $find_rombel->rombongan_belajar_id, 
				'siswa_id' 					=> $create_siswa->siswa_id,
				'last_sync'					=> date('Y-m-d H:i:s'),
			);
			$create_anggota_rombel = Anggota_rombel::updateOrCreate(
				['anggota_rombel_id_dapodik' => $data->anggota_rombel_id, 'semester_id' => $semester->semester_id],
				$insert_anggota_rombel
			);
			$i++;
		}
	}
	private function siswa_keluar($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'peserta didik keluar';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_siswa_keluar.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_siswa_keluar.json', json_encode($record));
			$find_siswa = Siswa::where('siswa_id_dapodik', '=', $data->peserta_didik_id)->first();
			if($find_siswa){
				$find_anggota_rombel = Anggota_rombel::where('siswa_id' , '=', $find_siswa->siswa_id)->where('semester_id', '=', $semester->semester_id)->first();
				if($find_anggota_rombel){
					$find_anggota_rombel->delete();
				}
				/*$find_user = User::where('siswa_id', '=', $find_siswa->siswa_id)->first();
				if($find_user){
					$find_user->delete();
				}
				$find_siswa->delete();*/
			}
		}
	}
	private function pembelajaran($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'pembelajaran';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_pembelajaran.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_pembelajaran.json', json_encode($record));
			$rombongan_belajar = Rombongan_belajar::where('rombel_id_dapodik', '=', $data->rombongan_belajar_id)->first();
			$get_guru = Guru::where('guru_id_dapodik', '=', $data->ptk_id)->first();
			$insert_pembelajaran = array(
				'sekolah_id'				=> $sekolah->sekolah_id,
				'rombongan_belajar_id'		=> $rombongan_belajar->rombongan_belajar_id,
				'guru_id'					=> $get_guru->guru_id,
				'mata_pelajaran_id'			=> $data->mata_pelajaran_id,
				'nama_mata_pelajaran'		=> $data->nama_mata_pelajaran,
				'kkm'						=> 0,
				'is_dapodik'				=> 1,
				'last_sync'					=> date('Y-m-d H:i:s'),
			);
			Pembelajaran::updateOrCreate(
				['pembelajaran_id_dapodik' => $data->pembelajaran_id, 'semester_id' => $semester->semester_id],
				$insert_pembelajaran
			);
			$i++;
		}
	}
	private function ekskul($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'ekstrakurikuler';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_ekskul.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_ekskul.json', json_encode($record));
			$get_wali = Guru::where('guru_id_dapodik', $data->ptk_id)->first();
			$insert_rombel = array(
				'sekolah_id' 			=> $sekolah->sekolah_id,
				'kurikulum_id' 			=> $data->kurikulum_id,
				'nama' 					=> $data->nama,
				'guru_id' 				=> $get_wali->guru_id,
				'tingkat' 				=> $data->tingkat_pendidikan_id,
				'guru_id_dapodik' 		=> $data->ptk_id,
				'rombel_id_dapodik'		=> $data->rombongan_belajar_id,
				'jenis_rombel'			=> $data->jenis_rombel,
				'last_sync'				=> date('Y-m-d H:i:s'),
			);
			$create_rombel = Rombongan_belajar::updateOrCreate(
				['rombel_id_dapodik' => $data->rombongan_belajar_id, 'semester_id' => $semester->semester_id],
				$insert_rombel
			);
			$insert_ekskul = array(
				'sekolah_id'	=> $sekolah->sekolah_id,
				'guru_id' => $get_wali->guru_id,
				'nama_ekskul' => $data->nm_ekskul,
				'is_dapodik' => 1,
				'rombongan_belajar_id'	=> $create_rombel->rombongan_belajar_id,
				'alamat_ekskul' => $data->nama_prasarana, 
				'last_sync'	=> date('Y-m-d H:i:s'),
			);
			Ekstrakurikuler::updateOrCreate(
				['id_kelas_ekskul' => $data->ID_kelas_ekskul, 'semester_id' => $semester->semester_id],
				$insert_ekskul
			);
			$i++;
		}
	}
	private function anggota_ekskul($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'anggota ekstrakurikuler';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_anggota_ekskul.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_anggota_ekskul.json', json_encode($record));
			$find_rombel = Rombongan_belajar::where('rombel_id_dapodik', '=', $data->rombongan_belajar_id)->first();
			$find_siswa = Siswa::where('siswa_id_dapodik', '=', $data->peserta_didik_id)->first();
			$insert_anggota_ekskul = array(
				'sekolah_id'				=> $sekolah->sekolah_id,
				'semester_id' 				=> $semester->semester_id, 
				'rombongan_belajar_id' 		=> $find_rombel->rombongan_belajar_id, 
				'siswa_id' 					=> $find_siswa->siswa_id,
				'anggota_rombel_id_dapodik'	=> $data->anggota_rombel_id,
				'last_sync'			=> date('Y-m-d H:i:s'),
			);
			$create_anggota_rombel = Anggota_rombel::updateOrCreate(
				['anggota_rombel_id_dapodik' => $data->anggota_rombel_id, 'semester_id' => $semester->semester_id],
				$insert_anggota_ekskul
			);
			$i++;
		}
	}
	private function dudi($response){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$dapodik = HelperServiceProvider::array_to_object($response);
		$i=1;
		$record['table'] = 'relasi dunia usaha dan industri (DUDI)';
		$record['jumlah'] = count((array)$dapodik->data);
		$record['inserted'] = $i;
		Storage::disk('public')->put('proses_dudi.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $i;
			Storage::disk('public')->put('proses_dudi.json', json_encode($record));
			$insert_dudi = array(
				'sekolah_id'		=> $data->sekolah_id,
				'nama'				=> $data->nama,
				'bidang_usaha_id'	=> $data->bidang_usaha_id,
				'nama_bidang_usaha'	=> $data->nama_bidang_usaha,
				'alamat_jalan'		=> $data->alamat_jalan,
				'rt'				=> $data->rt,
				'rw'				=> $data->rw,
				'nama_dusun'		=> $data->nama_dusun,
				'desa_kelurahan'	=> $data->desa_kelurahan,
				'kode_wilayah'		=> $data->kode_wilayah,
				'kode_pos'			=> $data->kode_pos,
				'lintang'			=> $data->lintang,
				'bujur'				=> $data->bujur,
				'nomor_telepon'		=> $data->nomor_telepon,
				'nomor_fax'			=> $data->nomor_fax,
				'email'				=> $data->email,
				'website'			=> $data->website,
				'npwp'				=> $data->npwp,
				'last_sync'			=> date('Y-m-d H:i:s'),
			);
			$create_dudi = Dudi::updateOrCreate(
				['dudi_id_dapodik' => $data->dudi_id],
				$insert_dudi
			);
			$insert_mou = array(
				'id_jns_ks'			=> $data->id_jns_ks,
				'dudi_id'			=> $create_dudi->dudi_id,
				'dudi_id_dapodik'	=> $data->dudi_id,
				'sekolah_id'		=> $data->sekolah_id,
				'nomor_mou'			=> $data->nomor_mou,
				'judul_mou'			=> $data->judul_mou,
				'tanggal_mulai'		=> $data->tanggal_mulai,
				'tanggal_selesai'	=> $data->tanggal_selesai,
				'nama_dudi'			=> $data->nama_dudi,
				'npwp_dudi'			=> $data->npwp_dudi,
				'nama_bidang_usaha'	=> $data->nama_bidang_usaha,
				'telp_kantor'		=> $data->telp_kantor,
				'fax'				=> $data->fax,
				'contact_person'	=> $data->contact_person,
				'telp_cp'			=> $data->telp_cp,
				'jabatan_cp'		=> $data->jabatan_cp,
				'last_sync'			=> date('Y-m-d H:i:s'),
			);
			$create_mou = Mou::updateOrCreate(
				['mou_id_dapodik' => $data->mou_id],
				$insert_mou
			);
			$i++;
		}
	}
	private function jurusan($response){
		$user = auth()->user();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$count_erapor = Jurusan::count();
		$count_server = count((array)$dapodik->data);
		$record['table'] = 'referensi jurusan';
		$record['jumlah'] = $count_server;
		$record['inserted'] = $count_erapor;
		Storage::disk('public')->put('proses_jurusan.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $count_erapor++;
			Storage::disk('public')->put('proses_jurusan.json', json_encode($record));
			$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
			$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
			$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
			$insert_jurusan = array(
				'nama_jurusan'			=> $data->nama_jurusan,
				'untuk_sma'				=> $data->untuk_sma,
				'untuk_smk'				=> $data->untuk_smk,
				'untuk_pt'				=> $data->untuk_pt,
				'untuk_slb'				=> $data->untuk_slb,
				'untuk_smklb'			=> $data->untuk_smklb,
				'jenjang_pendidikan_id'	=> $data->jenjang_pendidikan_id,
				'level_bidang_id'		=> $data->level_bidang_id,
				'created_at'			=> $data->created_at,
				'updated_at'			=> $data->updated_at,
				'deleted_at'			=> $data->deleted_at,
				'last_sync'				=> $data->last_sync,
			);
			//$query = Jurusan::where('jurusan_id', '=', $data->jurusan_induk)->first();
			//if($query){
				Jurusan::updateOrCreate(
					['jurusan_id' => $data->jurusan_id, 'jurusan_induk' => $data->jurusan_induk],
					$insert_jurusan
				);
			//}
		}
	}
	private function kurikulum($response){
		$user = auth()->user();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$count_erapor = Kurikulum::count();
		$count_server = count((array)$dapodik->data);
		$record['table'] = 'referensi kurikulum';
		$record['jumlah'] = $count_server;
		$record['inserted'] = $count_erapor;
		Storage::disk('public')->put('proses_kurikulum.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $count_erapor++;
			Storage::disk('public')->put('proses_kurikulum.json', json_encode($record));
			$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
			$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
			$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
			$insert_kurikulum = array(
				'nama_kurikulum'			=> $data->nama_kurikulum,
				'mulai_berlaku'				=> $data->mulai_berlaku,
				'sistem_sks'				=> $data->sistem_sks,
				'total_sks'					=> $data->total_sks,
				'jenjang_pendidikan_id'		=> $data->jenjang_pendidikan_id,
				'created_at'				=> $data->created_at,
				'updated_at'				=> $data->updated_at,
				'deleted_at'				=> $data->deleted_at,
				'last_sync'					=> $data->last_sync,
			);
			$query = Jurusan::where('jurusan_id', '=', $data->jurusan_id)->first();
			if($query){
				Kurikulum::updateOrCreate(
					['kurikulum_id' => $data->kurikulum_id, 'jurusan_id' => $data->jurusan_id],
					$insert_kurikulum
				);
			}
		}
	}
	private function mata_pelajaran($response){
		$user = auth()->user();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$count_erapor = Mata_pelajaran::count();
		$count_server = count((array)$dapodik->data);
		$record['table'] = 'referensi mata pelajaran';
		$record['jumlah'] = $count_server;
		$record['inserted'] = $count_erapor;
		Storage::disk('public')->put('proses_mata_pelajaran.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $count_erapor;
			Storage::disk('public')->put('proses_mata_pelajaran.json', json_encode($record));
			$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
			$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
			$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
			$insert_mata_pelajaran = array(
				'nama'						=> $data->nama,
				'pilihan_sekolah'			=> $data->pilihan_sekolah,
				'pilihan_kepengawasan'		=> $data->pilihan_kepengawasan,
				'pilihan_buku'				=> $data->pilihan_buku,
				'pilihan_evaluasi'			=> $data->pilihan_evaluasi,
				'created_at'				=> $data->created_at,
				'updated_at'				=> $data->updated_at,
				'deleted_at'				=> $data->deleted_at,
				'last_sync'					=> $data->last_sync,
			);
			$query = Jurusan::where('jurusan_id', '=', $data->jurusan_id)->first();
			if($query){
				Mata_pelajaran::updateOrCreate(
					['mata_pelajaran_id' => $data->mata_pelajaran_id, 'jurusan_id' => $data->jurusan_id],
					$insert_mata_pelajaran
				);
			}
		}
	}
	private function mapel_kur($response){
		$user = auth()->user();
		$dapodik = HelperServiceProvider::array_to_object($response);
		$count_erapor = Mata_pelajaran_kurikulum::count();
		$count_server = count((array)$dapodik->data);
		$record['table'] = 'referensi mata pelajaran kurikulum';
		$record['jumlah'] = $count_server;
		$record['inserted'] = $count_erapor;
		Storage::disk('public')->put('proses_mapel_kur.json', json_encode($record));
		if(!$dapodik->data){
			exit;
		}
		foreach($dapodik->data as $data){
			$record['inserted'] = $count_erapor++;
			Storage::disk('public')->put('proses_mapel_kur.json', json_encode($record));
			$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
			$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
			$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
			$insert_mata_pelajaran_kurikulum = array(
				'jumlah_jam'			=> $data->jumlah_jam,
				'jumlah_jam_maksimum'	=> $data->jumlah_jam_maksimum,
				'wajib'					=> $data->wajib,
				'sks'					=> $data->sks,
				'a_peminatan'			=> $data->a_peminatan,
				'area_kompetensi'		=> $data->area_kompetensi,
				'gmp_id'				=> $data->gmp_id,
				'created_at'			=> $data->created_at,
				'updated_at'			=> $data->updated_at,
				'deleted_at'			=> $data->deleted_at,
				'last_sync'				=> $data->last_sync
			);
			//$query = Kurikulum::where('kurikulum_id', '=', $data->kurikulum_id)->first();
			//if($query){
				Mata_pelajaran_kurikulum::updateOrCreate(
					['kurikulum_id' => $data->kurikulum_id, 'mata_pelajaran_id' => $data->mata_pelajaran_id, 'tingkat_pendidikan_id' => $data->tingkat_pendidikan_id],
					$insert_mata_pelajaran_kurikulum
				);
			//}
		}
	}
}
