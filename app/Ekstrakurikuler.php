<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
class Ekstrakurikuler extends Model
{
    use Uuid;
    public $incrementing = false;
	protected $table = 'ekstrakurikuler';
	protected $primaryKey = 'ekstrakurikuler_id';
	protected $guarded = [];
	public function rombongan_belajar(){
		return $this->hasOne('App\Rombongan_belajar', 'rombongan_belajar_id', 'rombongan_belajar_id');
	}
	public function guru(){
		return $this->hasOne('App\Guru', 'guru_id', 'guru_id');
	}
}
