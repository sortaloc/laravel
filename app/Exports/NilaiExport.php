<?php

namespace App\Exports;

use App\Kd_nilai;
use App\Rencana_penilaian;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
class NilaiExport implements FromView
{
	use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
	/*public function __construct(string $rencana_penilaian_id){
		$this->rencana_penilaian_id = $rencana_penilaian_id;
	}*/
	public function rencana_penilaian_id(string $rencana_penilaian_id)
    {
        $this->rencana_penilaian_id = $rencana_penilaian_id;
        
        return $this;
    }
	public function view(): View
    {
        return view('penilaian.export', [
            'rencana_penilaian_id' => $this->rencana_penilaian_id,
			'rencana_penilaian' => Rencana_penilaian::with(['kd_nilai.nilai', 'pembelajaran.anggota_rombel', 'pembelajaran', 'pembelajaran.anggota_rombel', 'pembelajaran.rombongan_belajar', 'pembelajaran.anggota_rombel.siswa'])->where('rencana_penilaian_id', '=', $this->rencana_penilaian_id)->first(),
        ]);
    }
}
