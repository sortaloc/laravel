<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gelar extends Model
{
    protected $table = 'ref_gelar';
	protected $primaryKey = 'gelar_akademik_id';
	protected $fillable = [
        'kode', 'nama', 'posisi_gelar', 'last_sync'
    ];
}
