<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
class Gelar_ptk extends Model
{
    use Uuid;
    public $incrementing = false;
	protected $table = 'gelar_ptk';
	protected $primaryKey = 'gelar_ptk_id';
	protected $fillable = [
        'gelar_ptk_id', 'sekolah_id', 'gelar_akademik_id', 'guru_id', 'ptk_id', 'last_sync'
    ];
}
