<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Semester;
use App\Setting;
use App\Guru;
use App\Sekolah;
use App\Providers\HelperServiceProvider;
class ConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
		$user = auth()->user();
		$jenis_gtk = HelperServiceProvider::jenis_gtk('guru');
		$data['all_guru']= Guru::where('sekolah_id', '=', $user->sekolah_id)->whereIn('jenis_ptk_id', $jenis_gtk)->get();
		$data['all_data'] = Semester::orderBy('semester_id', 'asc')->get();
		$data['sekolah_id'] = $user->sekolah_id;
		return view('config', $data);
    }
	public function simpan(Request $request){
		$this->validate($request,[
           'tanggal_rapor' 	=> 'required',
           'zona' 			=> 'required',
		   'semester_id' 	=> 'required',
		   'guru_id'		=> 'required',
		   'sekolah_id'		=> 'required',
        ]);
		/*$setting = Setting::find(1);
		$update = array(
			'tanggal_rapor' => $request['tanggal_rapor'],
           	'zona' 			=> $request['zona'],
		);*/
		Setting::where('key', '=', 'tanggal_rapor')->update(['value' => $request['tanggal_rapor']]);
		Setting::where('key', '=', 'zona')->update(['value' => $request['zona']]);
		Sekolah::find($request['sekolah_id'])->update(['guru_id' => $request['guru_id']]);
		Semester::where('periode_aktif', '=', 1)->update(['periode_aktif' => 0]);
		Semester::find($request['semester_id'])->update(['periode_aktif' => 1]);
        //return view('proses',['data' => $request]);
		/*$role = Role::create([
            'name' => $request['name'],
            'display_name' => $request['name'],
            'description' => $request['description'],
        ]);*/
		return redirect()->route('konfigurasi')->with('success', "Konfigurasi berhasil disimpan");
	}
}
