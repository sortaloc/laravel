<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guru;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Providers\HelperServiceProvider;
class GuruController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
		$data['query'] = 'guru';
		$data['title'] = 'Guru';
		return view('guru.list_guru', $data);
    }
	public function list_guru($query){
		$jenis_gtk = HelperServiceProvider::jenis_gtk($query);
		$user = auth()->user();
		$query = Guru::where('sekolah_id', '=', $user->sekolah_id)->whereIn('jenis_ptk_id', $jenis_gtk)->get();
		return DataTables::of($query)
			->addColumn('set_nama', function ($item) {
				$return  = $item->nama."<br />".$item->nuptk;
				return $return;
			})
			->addColumn('set_tempat_lahir', function ($item) {
				$return  = $item->tempat_lahir.', '.HelperServiceProvider::TanggalIndo(date('Y-m-d', strtotime($item->tanggal_lahir)));
				return $return;
			})
			->addColumn('actions', function ($item) {
				$user = auth()->user();
				if($user->hasRole('admin')){
				$links = '<div class="text-center"><div class="btn-group">
							<button type="button" class="btn btn-default btn-sm">Aksi</button>
							<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu pull-right text-left" role="menu">
								<li><a href="'.url('guru/view/'.$item->guru_id).'" class="toggle-modal"><i class="fa fa-eye"></i> Detil</a></li>
								<li><a href="'.url('guru/edit-gelar/'.$item->guru_id).'" class="toggle-modal"><i class="fa fa-pencil"></i> Edit Gelar</a></li>
							</ul>
						</div></div>';
				} else {
					$links = '<div class="text-center"><a class="btn btn-primary btn-sm" href="'.url('guru/view/'.$item->guru_id).'" class="toggle-modal"><i class="fa fa-eye"></i> Detil</a></div>';
				}
				//$links = $item->guru_id;
                return $links;

            })
            ->rawColumns(['actions', 'set_nama', 'set_tempat_lahir'])
            ->make(true);  
	}
	public function view($guru_id){
		$data['guru'] = Guru::find($guru_id);
		$data['title'] = 'Detil Guru';
		return view('guru.view', $data);
	}
	public function edit_gelar($guru_id){
		$data['guru'] = Guru::find($guru_id);
		$data['title'] = 'Edit Gelar Guru';
		return view('guru.edit', $data);
	}
	public function tendik(){
		$data['query'] = 'tendik';
		$data['title'] = 'Tenaga Kependidikan';
		return view('guru.list_guru', $data);
	}
	public function instruktur(){
		$data['query'] = 'instruktur';
		$data['title'] = 'Instruktur';
		return view('guru.list_guru', $data);
	}
	public function asesor(){
		$data['query'] = 'asesor';
		$data['title'] = 'Asesor';
		return view('guru.list_guru', $data);
	}
}
