<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Semester;
use App\Providers\HelperServiceProvider;
use App\Nilai;
use App\Rencana_penilaian;
use App\Exports\NilaiExport;
use App\Imports\NilaiImport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use File;
use App\Nilai_sikap;
use Yajra\Datatables\Datatables;
class PenilaianController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($kompetensi_id){
		$user = auth()->user();
		$semester = Semester::where('periode_aktif', 1)->first();
		$params = array(
			'user' => $user,
			'semester'	=> $semester,
			'title'	=> ucfirst($kompetensi_id),
			'kompetensi_id'	=> ($kompetensi_id == 'keterampilan') ? 2 : 1,
			'query'	=> $kompetensi_id,
		);
		return view('penilaian.form_penilaian')->with($params);
    }
	public function list_sikap (){
		$params = array(
			'title'	=> 'Data Penilaian Sikap',
		);
		return view('penilaian.list_sikap')->with($params);
    }
	public function get_list_sikap(Request $request){
		$user = auth()->user();
		$semester = HelperServiceProvider::get_ta();
		$callback = function($query) use ($user, $semester){
			$query->with('rombongan_belajar');
			$query->with('siswa');
			$query->where('sekolah_id', '=', $user->sekolah_id);
			$query->where('semester_id', '=', $semester->semester_id);
		};
		$query = Nilai_sikap::with('ref_sikap')->whereHas('anggota_rombel', $callback)->with(['anggota_rombel' => $callback]);
		return Datatables::of($query)
		->addColumn('nama_siswa', function ($item) {
			$return  = $item->anggota_rombel->siswa->nama;
			return $return;
		})
		->addColumn('nama_rombel', function ($item) {
			$return  = $item->anggota_rombel->rombongan_belajar->nama.'/'.$item->anggota_rombel->rombongan_belajar->tingkat;
			return $return;
		})
		->addColumn('get_butir_sikap', function ($item) {
			//dd($item->butir_sikap);
			$return  = $item->ref_sikap->butir_sikap;
			return $return;
		})
		->addColumn('get_opsi_sikap', function ($item) {
			$return  = ($item->opsi_sikap == 1) ? 'Positif' : 'Negatif';
			return $return;
		})
		->rawColumns(['nama_siswa', 'nama_rombel', 'get_butir_sikap', 'get_opsi_sikap'])
		->make(true);
	}
	public function simpan_nilai(Request $request){
		$user = auth()->user();
		$query = $request['query'];
		$siswa_id = $request['siswa_id'];
		$jumlah_kd = $request['jumlah_kd'];
		$bobot = $request['bobot_kd'];
		$total_bobot = $request['all_bobot'];
		$kompetensi_id = $request['kompetensi_id'];
		$kds = $request['kd'];
		$bobot = ($bobot > 0) ? $bobot : 1;
		$tanggal_sikap = $request['tanggal_sikap'];
		$butir_sikap = $request['butir_sikap'];
		$opsi_sikap = $request['opsi_sikap'];
		$uraian_sikap = $request['uraian_sikap'];
		$guru_id = $request['guru_id'];
		$output['jumlah_form'] = count($siswa_id);
		$redirect = ($kompetensi_id == 1) ? 'pengetahuan' : 'keterampilan';
		$insert = 0;
		$update = 0;
		if($query == 'sikap'){
			$tanggal_sikap = date('Y-m-d', strtotime($tanggal_sikap));
			$insert_sikap = array(
				'sekolah_id'		=> $user->sekolah_id,
				'guru_id'			=> $guru_id,
				'anggota_rombel_id'	=> $siswa_id,
				'tanggal_sikap' 	=> $tanggal_sikap,
				'butir_sikap'		=> $butir_sikap,
				'opsi_sikap'		=> $opsi_sikap,
				'uraian_sikap'		=> $uraian_sikap,
				'last_sync'			=> date('Y-m-d H:i:s'),
			);
			$create_nilai_sikap = Nilai_sikap::create($insert_sikap);
			if($create_nilai_sikap){
				$insert=1;
			}
			$redirect = '/list-sikap';
			$text = 'Data tidak disimpan. Periksa kembali semua isian';
		} elseif($query == 'remedial'){
			$redirect = '/remedial';
			$insert=1;
		} else {
			foreach($siswa_id as $k=>$siswa){
				foreach($kds as $key=>$kd) {
					$a = $this->check_100($kd, $redirect);
					if($a){
						$output['title'] = 'Gagal';
						$output['text'] = $a;
						$output['icon'] = 'error';
						$output['redirect'] = '';
						echo json_encode($output);
						exit;
					}
				}
			}
			foreach($siswa_id as $k=>$siswa){
				$hitung = 0;
				foreach($kds as $kd){
					$hitung += $kd[$k];
				}
				$hasil = $hitung/$jumlah_kd;
				$rerata_nilai = $hasil*$bobot;//($hasil*$bobot)/$total_bobot;
				$rerata_jadi = number_format($rerata_nilai/$total_bobot,2);
				$rerata_stor[] = number_format($hitung/$jumlah_kd,0);
				$record['value'] 	= number_format($hitung/$jumlah_kd,0);
				//=F6*(C4/(C4+G4+J4+M4))
				$record['rerata_text'] 	= 'x '.$bobot.' / '.$total_bobot.' =';
				$record['rerata_jadi'] 	= $rerata_jadi;
				$output['rerata'][] = $record;
			}
			foreach($siswa_id as $k=>$siswa){
				foreach($kds as $key=>$kd) {
					$nilai = ($kd[$k]) ? $kd[$k] : 0;
					if($nilai){
						$get_nilai = Nilai::where('kd_nilai_id', '=', $key)->where('anggota_rombel_id', '=', $siswa)->first();
						if($get_nilai){
							$update++;
							$get_nilai->nilai = $nilai;
							$get_nilai->rerata = $rerata_stor[$k];
							$get_nilai->last_sync = date('Y-m-d H:i:s');
							$get_nilai->save();
						} else {
							$insert++;
							$insert_nilai = array(
								'sekolah_id'		=> $user->sekolah_id,
								'kd_nilai_id'		=> $key,
								'anggota_rombel_id'	=> $siswa,
								'kompetensi_id'		=> $kompetensi_id,
								'nilai'				=> $nilai,
								'rerata'			=> $rerata_stor[$k],
								'last_sync'			=> date('Y-m-d H:i:s')
							);
							Nilai::create($insert_nilai);
						}
					}
				}
			}
			$redirect = ($kompetensi_id == 1) ? '/pengetahuan' : '/keterampilan';
			$text = 'Tidak ada nilai disimpan. Periksa kembali isian nilai KD';
		}
		$output['rumus'] = '';
		if($insert || $update){
			$output['title'] = 'Berhasil';
			$output['text'] = 'Nilai berhasil disimpan';
			$output['icon'] = 'success';
			$output['redirect'] = $redirect;
		} else {
			$output['title'] = 'Gagal';
			$output['text'] = $text;
			$output['icon'] = 'error';
			$output['redirect'] = '';
		}
		echo json_encode($output);
	}
	static function check_100($array, $redirect){
		$return = '';
		foreach ($array as $a) {
			if($a){
				if (is_numeric($a)) {
					if($a < 0){
						$return = 'Tambah data nilai '.$redirect.' gagal. Nilai tidak boleh minus';
					} elseif($a > 100){
						$return = 'Tambah data nilai '.$redirect.' gagal. Nilai harus tidak lebih besar dari 100';
					}
				} else {
					$return = 'Tambah data nilai '.$redirect.' gagal. Nilai harus berupa angka';
				}
			}
		}
		return $return;
	}
	public function exportToExcel($id){
		$rencana_penilaian = Rencana_penilaian::with(['pembelajaran.rombongan_belajar'])->where('rencana_penilaian_id', '=', $id)->first();
		$kompetensi = ($rencana_penilaian->kompetensi_id == 1) ? 'Pengetahuan' : 'Keterampilan';
		$nama_file = 'Format Nilai '.$kompetensi.' eRaporSMK '.$rencana_penilaian->pembelajaran->nama_mata_pelajaran.' '.$rencana_penilaian->pembelajaran->rombongan_belajar->nama.'.xlsx';
		return (new NilaiExport)->rencana_penilaian_id($id)->download($nama_file);
	}
	public function import_excel(Request $request){
		$validator = Validator::make($request->all(), [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
		if ($validator->passes()) {
			$file = $request->file('file');
			$nama_file = rand().$file->getClientOriginalName();
			$file->move('excel',$nama_file);
			$Import = new NilaiImport();
			$rows = Excel::import($Import, public_path('/excel/'.$nama_file));
			if(File::exists(public_path('/excel/'.$nama_file))) {
				File::delete(public_path('/excel/'.$nama_file));
			}
			return response()->json($Import);
		}
		return response()->json(['error'=>$validator->errors()->all()]);
	}
}
