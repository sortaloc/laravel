<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Role_user;
use App\Providers\HelperServiceProvider;
use Ixudra\Curl\Facades\Curl;
use App\Sekolah;
use App\Mst_wilayah;
use App\Guru;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Jurusan_sp;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use App\Mata_pelajaran;
use App\Mata_pelajaran_kurikulum;
use Chumper\Zipper\Facades\Zipper;
use App\Kd_json;
use App\Kompetensi_dasar;
use Artisan;
use Illuminate\Support\Facades\Schema;
use App\Setting;
use File;
use App\Jurusan;
use App\Kurikulum;
class SinkronisasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function index(){
		Storage::disk('public')->delete('proses_sekolah.json');
		Storage::disk('public')->delete('proses_guru.json');
		Storage::disk('public')->delete('proses_rombongan_belajar.json');
		Storage::disk('public')->delete('proses_siswa_aktif.json');
		Storage::disk('public')->delete('proses_siswa_keluar.json');
		Storage::disk('public')->delete('proses_pembelajaran.json');
		Storage::disk('public')->delete('proses_ekskul.json');
		Storage::disk('public')->delete('proses_anggota_ekskul.json');
		Storage::disk('public')->delete('proses_jurusan.json');
		Storage::disk('public')->delete('proses_kurikulum.json');
		Storage::disk('public')->delete('proses_mata_pelajaran.json');
		Storage::disk('public')->delete('proses_mapel_kur.json');
		Storage::disk('public')->delete('proses_dudi.json');
		Storage::disk('public')->delete('proses_anggota_ekskul_by_rombel.json');
		Storage::disk('public')->delete('proses_sinkron_sekolah.json');
		Storage::disk('public')->delete('proses_kompetensi_dasar.json');
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		$host_server = HelperServiceProvider::url_server_direktorat('status');
		$response = Curl::to($host_server)
        ->withData($data_sync)
        ->post();
		$host_erapor_server = 'http://103.40.55.226/dashboard_erapor/proses/count_kd';
		$response_dashboard = Curl::to($host_erapor_server)
        ->withData($data_sync)
        ->post();
		if($response && $response_dashboard){
			$response = json_decode($response);
			$response_dashboard = json_decode($response_dashboard);
			$response->ref_kd = $response_dashboard->ref_kd;
		}
		$data['semester'] = $semester;
		$data['data'] = $response;
		return view('sinkronisasi.index', $data);
    }
	public function sekolah(){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		$host_server = HelperServiceProvider::url_server_direktorat('sekolah');
		$response = Curl::to($host_server)
        ->withData($data_sync)
        ->post();
		$response = json_decode($response);
		if($response->post_login){
			$ptk = $response->kepsek;
			$kecamatan_kepsek = Mst_wilayah::where('kode_wilayah', $ptk->kode_wilayah)->first();
			$nuptk = ($ptk->nuptk) ? $ptk->nuptk : mt_rand();
			$insert_guru = array(
				'sekolah_id' 			=> $user->sekolah_id,
				'nama' 					=> $ptk->nama,
				'nuptk' 				=> $nuptk,
				'nip' 					=> $ptk->nip,
				'nik' 					=> $ptk->nik,
				'jenis_kelamin' 		=> $ptk->jenis_kelamin,
				'tempat_lahir' 			=> $ptk->tempat_lahir,
				'tanggal_lahir' 		=> $ptk->tanggal_lahir,
				'status_kepegawaian_id'	=> $ptk->status_kepegawaian_id,
				'jenis_ptk_id' 			=> $ptk->jenis_ptk_id,
				'agama_id' 				=> $ptk->agama_id,
				'alamat' 				=> $ptk->alamat_jalan,
				'rt' 					=> $ptk->rt,
				'rw' 					=> $ptk->rw,
				'desa_kelurahan' 		=> $ptk->desa_kelurahan,
				'kecamatan' 			=> $kecamatan_kepsek->nama,
				'kode_pos'				=> ($ptk->kode_pos) ? $ptk->kode_pos : 0,
				'no_hp'					=> ($ptk->no_hp) ? $ptk->no_hp : 0,
				'email' 				=> $ptk->email,
				'guru_id_dapodik' 		=> $ptk->ptk_id,
				'is_dapodik'			=> 1,
				'last_sync'				=> date('Y-m-d H:i:s'),
			);
			$find = Guru::where('guru_id_dapodik', $ptk->ptk_id)->first();
			if($find){
				$guru_id = $find->guru_id;
				Guru::find($find->guru_id)->update($insert_guru);
			} else {
				$guru = Guru::create($insert_guru);
				$guru_id = $guru->guru_id;
			}
			$random = Str::random(6);
			$email_ptk = ($ptk->email) ? $ptk->email : strtolower($random).'@eraporsmk.net';
			if($guru_id){
				$find_user = User::where('email', $email_ptk)->first();
				if(!$find_user){
					$create_user = User::create([
						'name' => $ptk->nama,
						'email' => $email_ptk,
						'nuptk'	=> $nuptk,
						'password' => Hash::make(12345678),
						'last_sync'	=> date('Y-m-d H:i:s'),
						'sekolah_id'	=> $user->sekolah_id,
						'password_dapo'	=> md5(12345678),
						'guru_id'	=> $guru_id,
					]);
					if($create_user){
						$adminRole = Role::where('name', 'guru')->first();
						$CheckadminRole = DB::table('role_user')->where('user_id', $create_user->user_id)->first();
						if(!$CheckadminRole){
							$create_user->attachRole($adminRole);
						}
					}
				} else {
					$adminRole = Role::where('name', 'guru')->first();
					$CheckadminRole = DB::table('role_user')->where('user_id', $find_user->user_id)->first();
					if(!$CheckadminRole){
						$find_user->attachRole($adminRole);
					}
				}
			}
			foreach($response->jurusan_sp as $jurusan_sp){
				$insert_jur_sp = array(
					'sekolah_id'	=> $user->sekolah_id,
					'jurusan_id'	=> $jurusan_sp->jurusan_id,
					'nama_jurusan_sp'	=> $jurusan_sp->nama_jurusan_sp,
					'jurusan_sp_id_dapodik' => $jurusan_sp->jurusan_sp_id,
					'last_sync'	=> date('Y-m-d H:i:s'),
				);
				$get_jurusan_sp = Jurusan_sp::where('jurusan_sp_id_dapodik', $jurusan_sp->jurusan_sp_id)->first();
				if($get_jurusan_sp){
					Jurusan_sp::find($get_jurusan_sp->jurusan_sp_id)->update($insert_guru);
				} else {
					Jurusan_sp::create($insert_jur_sp);
				}
			}
			$host_erapor_server = 'http://103.40.55.226/dashboard_erapor/proses/sekolah';
			$response_dashboard = Curl::to($host_erapor_server)
			->withData($data_sync)
			->post();
			$response_dashboard = json_decode($response_dashboard);
			if($response_dashboard){
				if($response_dashboard->sekolah && $response_dashboard->update){
					$update_sekolah = (array) $response->data_sekolah;
					$update_sekolah = array_merge($update_sekolah, array('sinkron' => 1, 'guru_id' => $guru_id));
					$flash['success'] = 'Data Sekolah berhasil di sinkronisasi. Sekolah ditemukan di server dashboard dan berhasil mengirim data ke server dashboard';
					Sekolah::find($user->sekolah_id)->update($update_sekolah);
				} elseif(!$response_dashboard->sekolah && $response_dashboard->update){
					$flash['success'] = 'Data Sekolah berhasil di sinkronisasi. Sekolah tidak ditemukan di server dashboard. Silahkan menghubungi Tim Helpdesk';
				} elseif($response_dashboard->sekolah && !$response_dashboard->update){
					$flash['success'] = 'Data Sekolah berhasil di sinkronisasi. Gagal mengirim data sekolah ke server dashboard. Silahkan menghubungi Tim Helpdesk';
				} else {
					$flash['success'] = 'Data Sekolah berhasil di sinkronisasi. Sekolah tidak ditemukan di server dashboard dan data sekolah tidak terkirim ke server dashboard. Silahkan menghubungi Tim Helpdesk';
				}
			} else {
				$flash['success'] = 'Data Sekolah berhasil di sinkronisasi, namun perlu disinkronisasi ulang';
			}
		} else {
			$flash['error'] = 'Data Sekolah gagal di sinkronisasi';
		}
		return redirect()->route('ambil_data')->with($flash);
	}
	public function guru(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('guru.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('guru_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('guru.json', $response);
			return redirect()->route('sinkronisasi_guru');
		}
		$json = Storage::disk('public')->get('guru.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		return view('sinkronisasi.guru', $data);
	}
	public function rombongan_belajar(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('rombongan_belajar.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('rombongan_belajar_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('rombongan_belajar.json', $response);
			return redirect()->route('sinkronisasi_rombel');
		}
		$json = Storage::disk('public')->get('rombongan_belajar.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.rombongan_belajar', $data);
	}
	public function siswa_aktif(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('siswa_aktif.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('siswa_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('siswa_aktif.json', $response);
			return redirect()->route('sinkronsasi_siswa_aktif');
		}
		$json = Storage::disk('public')->get('siswa_aktif.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.siswa_aktif', $data);
	}
	public function siswa_keluar(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('siswa_keluar.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('siswa_keluar_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('siswa_keluar.json', $response);
			return redirect()->route('sinkronsasi_siswa_keluar');
		}
		$json = Storage::disk('public')->get('siswa_keluar.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.siswa_keluar', $data);
	}
	public function pembelajaran(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('pembelajaran.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('pembelajaran_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('pembelajaran.json', $response);
			return redirect()->route('sinkronisasi_pembelajaran');
		}
		$json = Storage::disk('public')->get('pembelajaran.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.pembelajaran', $data);
	}
	public function ekskul(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('ekskul.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('ekskul_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('ekskul.json', $response);
			return redirect()->route('sinkronisasi_ekskul');
		}
		$json = Storage::disk('public')->get('ekskul.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.ekskul', $data);
	}
	public function anggota_ekskul(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('anggota_ekskul.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('anggota_ekskul_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('anggota_ekskul.json', $response);
			return redirect()->route('sinkronisasi_anggota_ekskul');
		}
		$json = Storage::disk('public')->get('anggota_ekskul.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.anggota_ekskul', $data);
	}
	public function dudi(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn
		);
		if(!Storage::disk('public')->exists('dudi.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('dudi_sync');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('dudi.json', $response);
			return redirect()->route('sinkronisasi_dudi');
		}
		$json = Storage::disk('public')->get('dudi.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.dudi', $data);
	}
	public function anggota_by_rombel(Request $request, $rombongan_belajar_id){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'sekolah_id'			=> $user->sekolah_id,
			'semester_id'			=> $semester->semester_id,
			'rombongan_belajar_id'	=> $rombongan_belajar_id
		);
		if(!Storage::disk('public')->exists('anggota_ekskul_by_rombel.json')){
			$host_server = HelperServiceProvider::url_server_direktorat('anggota_ekskul_sync_by_ekskul_id');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('anggota_ekskul_by_rombel.json', $response);
			return redirect(url('/sinkronisasi/anggota-by-rombel/'.$rombongan_belajar_id));
		}
		$json = Storage::disk('public')->get('anggota_ekskul_by_rombel.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = $semester;
		return view('sinkronisasi.anggota_ekskul_by_rombel', $data);
	}
	public function jurusan(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		if(!Storage::disk('public')->exists('jurusan.json')){
			$jurusan = App\Jurusan::orderBy('updated_at', 'desc')->first();
			$data_sync = array(
				'updated_at'		=> $jurusan->updated_at,
			);
			$host_server = HelperServiceProvider::url_server_direktorat('jurusan');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('jurusan.json', $response);
			return redirect()->route('sinkronisasi_jurusan');
		}
		$json = Storage::disk('public')->get('jurusan.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.jurusan', $data);
	}
	public function kurikulum(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		if(!Storage::disk('public')->exists('kurikulum.json')){
			$kurikulum = App\Kurikulum::orderBy('updated_at', 'desc')->first();
			$data_sync = array(
				'updated_at'		=> $kurikulum->updated_at,
			);
			$host_server = HelperServiceProvider::url_server_direktorat('kurikulum');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('kurikulum.json', $response);
			return redirect()->route('sinkronisasi_kurikulum');
		}
		$json = Storage::disk('public')->get('kurikulum.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.kurikulum', $data);
	}
	public function mata_pelajaran(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		if(!Storage::disk('public')->exists('mata_pelajaran.json')){
			$mata_pelajaran = Mata_pelajaran::orderBy('updated_at', 'desc')->first();
			$data_sync = array(
				'updated_at'		=> ($mata_pelajaran) ? $mata_pelajaran->updated_at->toDateTimeString() : '2000-01-01 17:00:00'
			);
			$host_server = HelperServiceProvider::url_server_direktorat('mata_pelajaran');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('mata_pelajaran.json', $response);
			return redirect()->route('sinkronisasi_mata_pelajaran');
		}
		$json = Storage::disk('public')->get('mata_pelajaran.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 10;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.mata_pelajaran', $data);
	}
	public function mapel_kur(Request $request){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		if(!Storage::disk('public')->exists('mapel_kur.json')){
			$mata_pelajaran_kurikulum = Mata_pelajaran_kurikulum::orderBy('updated_at', 'desc')->first();
			$data_sync = array(
				'updated_at'		=> ($mata_pelajaran_kurikulum) ? $mata_pelajaran_kurikulum->updated_at->toDateTimeString() : '2000-01-01 17:00:00'
			);
			$host_server = HelperServiceProvider::url_server_direktorat('mapel_kur');
			$response = Curl::to($host_server)
			->withData($data_sync)
			->post();
			Storage::disk('public')->put('mapel_kur.json', $response);
			return redirect()->route('sinkronisasi_mapel_kur');
		}
		$json = Storage::disk('public')->get('mapel_kur.json');
		$response = json_decode($json);
		if($response->post_login){
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$itemCollection = collect($response->data);
			$perPage = 100;
			$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
			$paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
			$paginatedItems->setPath($request->url());
			$data['dapodik'] = $paginatedItems;
			$data['currentPage'] = $currentPage;
		} else {
			$data['dapodik'] = array();
			$data['currentPage'] = 0;
		}
		$data['user'] = $user;
		$data['sekolah'] = $sekolah;
		$data['per_page'] = $perPage;
		$data['semester'] = DB::table('semester')->where('periode_aktif', 1)->first();
		return view('sinkronisasi.mapel_kur', $data);
	}
	public function ref_kd(){
		$data['title'] = 'Sinkronisasi Kompetensi Dasar';
		$data['terproses']	= Kompetensi_dasar::count();
		return view('sinkronisasi.kompetensi_dasar', $data);
	}
	public function proses_artisan(){
		$response = Artisan::call('kd:start');
	}
	public function jumlah_kd(){
		echo 'Jumlah Ref.KD terproses : '.Kompetensi_dasar::count();
	}
	public function proses_kd($file){
		Kd_json::create(['nama_file' => $file]);
		$json = Storage::disk('public')->get('kd_json/kd_json/'.$file);
		$response = json_decode($json);
		foreach($response->data as $obj){
			$create_kd = Kompetensi_dasar::updateOrCreate(
				['aspek' => $obj->aspek, 'mata_pelajaran_id' => $obj->mata_pelajaran_id, 'kompetensi_dasar' => $obj->kompetensi_dasar, 'kurikulum_id' => $obj->kurikulum_id, 'kelas' => $obj->kelas],
				['id_kompetensi' => $obj->id_kompetensi, 'id_kompetensi_nas' => $obj->id_kompetensi_nas, 'kompetensi_dasar_alias' => $obj->kompetensi_dasar_alias, 'aktif' => $obj->aktif, 'created_at' => $obj->created_at, 'updated_at' => $obj->updated_at, 'deleted_at' => $obj->deleted_at, 'last_sync' => $obj->last_sync]
			);
		}
		$flash['success'] = 'Berhasil sinkonisasi sejumlah '.count($response->data).' referensi KD';
		return redirect()->route('sinkronisasi_ref_kd')->with($flash);
	}
	public function kirim_data(){
		$url_server = 'http://103.40.55.226/dashboard_erapor/proses/status';
		$url_server = 'http://localhost/dashboard_erapor/proses/status';
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$response = Curl::to($url_server)->get();
		$param = array(
			'user' 		=> $user,
			'sekolah' 	=> $sekolah,
			'semester' 	=> $semester,
			'status_sync'	=> json_decode($response),
		);
		return view('sinkronisasi.kirim_data')->with($param);
	}
	public function kirim_nilai(){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$param = array(
			'user' 		=> $user,
			'sekolah' 	=> $sekolah,
			'semester' 	=> $semester,
		);
		return view('sinkronisasi.kirim_nilai')->with($param);
	}
	public function proses_sync(){
		$session_id = session()->getId();
		$user = auth()->user();
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$last_sync = Setting::where('key', '=', 'last_sync')->first();
		$last_sync = $last_sync->value;
		$last_sync_date = date('Y-m-d', strtotime($last_sync));
		$last_sync_time = date('H:i:s', strtotime($last_sync));
		$url_server = 'http://103.40.55.226/dashboard_erapor/proses/proses_laravel';
		$url_server = 'http://localhost/dashboard_erapor/proses/proses_laravel';
		$table_sync = HelperServiceProvider::table_sync();
		$i=1;
		$total = 0;
		$result = 0;
		foreach($table_sync as $sync){
			if(Schema::hasTable($sync)){
				$query = DB::table($sync);
				if($sync == 'ref_kompetensi_dasar'){
					$query->whereNotNull('user_id');
				} elseif (Schema::hasColumn($sync, 'last_sync')) {
					//$query->where('last_sync', '>=', $last_sync);
					$query->whereDate('last_sync', '>=', $last_sync_date);
					$query->whereTime('last_sync', '>=', $last_sync_time);
				}
				if (Schema::hasColumn($sync, 'semester_id')){
					$query->where('semester_id', '=', $semester->semester_id);
				}
				if (Schema::hasColumn($sync, 'sekolah_id')){
					$query->where('sekolah_id', '=', $user->sekolah_id);
				}
				$result = $query->count();
				if($result){
					$total++;
				}
			}
		}
		$i=0;
		$limit = 10000;
		foreach($table_sync as $sync){
			if(Schema::hasTable($sync)){
				$query = DB::table($sync);
				if($sync == 'ref_kompetensi_dasar'){
					$query->whereNotNull('user_id');
				} elseif (Schema::hasColumn($sync, 'last_sync')) {
					//$query->where('last_sync', '>=', $last_sync);
					$query->whereDate('last_sync', '>=', $last_sync_date);
					$query->whereTime('last_sync', '>=', $last_sync_time);
				}
				if (Schema::hasColumn($sync, 'semester_id')){
					$query->where('semester_id', '=', $semester->semester_id);
				}
				if (Schema::hasColumn($sync, 'sekolah_id')){
					$query->where('sekolah_id', '=', $user->sekolah_id);
				}
				$count = $query->count();
				if($count){
					if($count > $limit){
						for ($counter = 0; $counter <= $count; $counter += $limit) {
							$query = DB::table($sync);
							if($sync == 'ref_kompetensi_dasar'){
								$query->whereNotNull('user_id');
							} elseif (Schema::hasColumn($sync, 'last_sync')) {
								//$query->where('last_sync', '>=', $last_sync);
								$query->whereDate('last_sync', '>=', $last_sync_date);
								$query->whereTime('last_sync', '>=', $last_sync_time);
							}
							if (Schema::hasColumn($sync, 'semester_id')){
								$query->where('semester_id', '=', $semester->semester_id);
							}
							if (Schema::hasColumn($sync, 'sekolah_id')){
								$query->where('sekolah_id', '=', $user->sekolah_id);
							}
							$query->offset($counter)->limit($limit);
							$result = $query->get();
							if($result){
								$withData = array(
									'semester_id' => $semester->semester_id,
									'sekolah_id' => $user->sekolah_id,
									'table' => $sync,
									'json' => HelperServiceProvider::prepare_send(json_encode($result)),
								);
								$kirim_data = Curl::to($url_server)->withData($withData)->post();
								$percent = intval($i/ $total * 100);
								$arr_content['percent'] = $percent;
								$arr_content['message'] = "Mengirim data " .$sync;
								$arr_content['total'] = $total;
								$arr_content['no'] = $i;
								$arr_content['response'] = $kirim_data;
								//file_put_contents("./assets/temp/" . $session_id . ".txt", json_encode($arr_content));
								Storage::disk('public')->put($session_id . ".txt", json_encode($arr_content));
							}
						}
						$i++;
					} else {
						$query = DB::table($sync);
						if($sync == 'ref_kompetensi_dasar'){
							$query->whereNotNull('user_id');
						} elseif (Schema::hasColumn($sync, 'last_sync')) {
							//$query->where('last_sync', '>=', $last_sync);
							$query->whereDate('last_sync', '>=', $last_sync_date);
							$query->whereTime('last_sync', '>=', $last_sync_time);
						}
						if (Schema::hasColumn($sync, 'semester_id')){
							$query->where('semester_id', '=', $semester->semester_id);
						}
						if (Schema::hasColumn($sync, 'sekolah_id')){
							$query->where('sekolah_id', '=', $user->sekolah_id);
						}
						$result = $query->get();
						if($result){
							$withData = array(
								'semester_id' => $semester->semester_id,
								'sekolah_id' => $user->sekolah_id,
								'table' => $sync,
								'json' => HelperServiceProvider::prepare_send(json_encode($result)),
							);
							$kirim_data = Curl::to($url_server)->withData($withData)->post();
							$percent = intval($i/ $total * 100);
							$arr_content['percent'] = $percent;
							$arr_content['message'] = "Mengirim data " .$sync;
							$arr_content['total'] = $total;
							$arr_content['no'] = $i;
							$arr_content['response'] = $kirim_data;
							Storage::disk('public')->put($session_id . ".txt", json_encode($arr_content));
							$i++;
						}
					}
				}
				sleep(1);
			}
		}
		$update_last_sync = Setting::where('key', '=', 'last_sync')->update(['value' => date('Y-m-d H:i:s')]);
		if($update_last_sync){
			$record['title'] 	= 'Sukses';
			$record['text'] 	= 'Sinkronisasi Selesai';
			$record['type']		= 'success';
		} else {
			$record['value'] 	= 'Gagal';
			$record['text'] 	= 'Sinkronisasi Gagal! Silahkan dicoba beberapa saat lagi';
			$record['type']		= 'error';
		}
		$output['result'] = $record;
		echo json_encode($record);
		//unlink("./assets/temp/" . $session_id . ".txt");
		if(File::exists($session_id . ".txt")) {
			File::delete($session_id . ".txt");
		}
	}
	public function proses_artisan_sync($server, $data, $aksi){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		/*$last_sync = Setting::where('key', '=', 'last_sync')->first();
		$last_sync = $last_sync->value;*/
		$last_sync = '1901-01-01 00:00:00';
		if($aksi == 'jurusan'){
			$query = Jurusan::orderBy('last_sync', 'DESC')->first();
			if($query){
				$last_sync = $query->last_sync;
			}
		} elseif($aksi == 'kurikulum'){
			$query = Kurikulum::orderBy('last_sync', 'DESC')->first();
			if($query){
				$last_sync = $query->last_sync;
			}
		} elseif($aksi == 'mata_pelajaran'){
			$query = Mata_pelajaran::orderBy('last_sync', 'DESC')->first();
			if($query){
				$last_sync = $query->last_sync;
			}
		} elseif($aksi == 'mapel_kur'){
			$query = Mata_pelajaran_kurikulum::orderBy('last_sync', 'DESC')->first();
			if($query){
				$last_sync = $query->last_sync;
			}
		}
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn,
			'server'			=> ($server == 'erapor_server') ? 'http://103.40.55.242/erapor_server/sync/' : 'http://103.40.55.226/dashboard_erapor/proses/',
			//'server'			=> ($server == 'erapor_server') ? 'http://localhost/erapor_server_api/sync/' : 'http://localhost/dashboard_erapor/proses/',
			'aksi'				=> $aksi,
			'last_sync'			=> $last_sync
		);
		Artisan::call('sinkronisasi:ambildata',$data_sync);
	}
	public function hitung_data($data){
		$file = 'proses_'.$data.'.json';
		if(!Storage::disk('public')->exists($file)){
			$record['table'] = $data;
			$record['jumlah'] = 0;
			$record['inserted'] = 0;
			Storage::disk('public')->put($file, json_encode($record));
		}
		Artisan::call('sinkronisasi:hitungdata', ['file' => 'proses_'.$data.'.json']);
		echo Artisan::output();
	}
	public function debug(){
		$user = auth()->user();
		$sekolah = Sekolah::find($user->sekolah_id);
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$data_sync = array(
			'username_dapo'		=> $user->email,
			'password_dapo'		=> trim($user->password_dapo),
			'tahun_ajaran_id'	=> $semester->tahun_ajaran_id,
			'semester_id'		=> $semester->semester_id,
			'sekolah_id'		=> $user->sekolah_id,
			'npsn'				=> $sekolah->npsn,
			'server'			=> 'http://103.40.55.242/erapor_server/sync/',
			'aksi'				=> 'rombongan_belajar_sync',
			'updated_at'		=> date('Y-m-d H:i:s'),
		);
		Artisan::call('sinkronisasi:ambildata',$data_sync);
	}
}
