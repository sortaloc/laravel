<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Siswa;
Use App\Rombongan_belajar;
Use App\Anggota_rombel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Providers\HelperServiceProvider;
use Illuminate\Support\Facades\DB;
use App\Jurusan_sp;
use Illuminate\Support\Str;
class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
		$user = auth()->user();
		$params = array(
			'status' => 'aktif',
			'title' => 'Peserta Didik Aktif',
			'all_jurusan' => Jurusan_sp::where('sekolah_id', '=', $user->sekolah_id)->get(),
		);
		return view('siswa.list_siswa')->with($params);
    }
	public function keluar(){
		$user = auth()->user();
		$params = array(
			'status' => 'keluar',
			'title' => 'Peserta Didik Keluar',
			'all_jurusan' => Jurusan_sp::where('sekolah_id', '=', $user->sekolah_id)->get(),
		);
		return view('siswa.list_siswa')->with($params);
    }
	public function list_siswa(Request $request, $status){
		$user = auth()->user();
		$semester = HelperServiceProvider::get_ta();
		if($status == 'aktif'){
			$query = Anggota_rombel::select(['rombongan_belajar.*', 'anggota_rombel.*', 'anggota_rombel.deleted_at as terhapus'])->where('anggota_rombel.sekolah_id', '=', $user->sekolah_id)->where('anggota_rombel.semester_id', '=', $semester->semester_id)
			->with('siswa')
			->join('rombongan_belajar', function ($join) {
				$join->on('anggota_rombel.rombongan_belajar_id', '=', 'rombongan_belajar.rombongan_belajar_id')->where('rombongan_belajar.jenis_rombel', '=', 1);
			})
			->get();
		} else {
			$query = Anggota_rombel::select(['rombongan_belajar.*', 'anggota_rombel.*', 'anggota_rombel.deleted_at as terhapus'])->where('anggota_rombel.sekolah_id', '=', $user->sekolah_id)->where('anggota_rombel.semester_id', '=', $semester->semester_id)
			->with('siswa')
			->join('rombongan_belajar', function ($join) {
				$join->on('anggota_rombel.rombongan_belajar_id', '=', 'rombongan_belajar.rombongan_belajar_id')->where('rombongan_belajar.jenis_rombel', '=', 1);
			})
			->onlyTrashed()->get();
		}
		return DataTables::of($query)
			->filter(function ($instance) use ($request) {
				if ($request->has('filter_jurusan')) {
					$instance->collection = $instance->collection->filter(function ($row) use ($request) {
						return Str::contains($row['jurusan_id'], $request->get('filter_jurusan')) ? true : false;
					});
				}
				if ($request->has('filter_kelas')) {
					$instance->collection = $instance->collection->filter(function ($row) use ($request) {
						return Str::contains($row['tingkat'], $request->get('filter_kelas')) ? true : false;
					});
				}
			})
			->addColumn('set_nama', function ($item) {
				if($item->siswa){
					if($item->siswa->photo){
						$foto = url('storage/images/'.$item->siswa->photo);
					} else {
						if($item->siswa->jenis_kelamin == 'L'){
							$foto = url('vendor/img/no_avatar.jpg');
						} else {
							$foto = url('vendor/img/no_avatar_f.jpg');
						}
					}
					$return  = '<img src="'.$foto.'" width="50" style="float:left; margin-right:10px;" />'.$item->siswa->nama."<br />".$item->siswa->nisn;
				} else {
					$return  = '-';
				}
				return $return;
			})
			->addColumn('set_tempat_lahir', function ($item) {
				$return  = ($item->siswa) ? $item->siswa->tempat_lahir.', '.HelperServiceProvider::TanggalIndo(date('Y-m-d', strtotime($item->siswa->tanggal_lahir))) : '-';
				return $return;
			})
			->addColumn('agama', function ($item) {
				$get_agama = ($item->siswa) ? DB::table('ref_agama')->find($item->siswa->agama_id) : '';
				$return  = ($get_agama) ? $get_agama->nama : '-';
				return $return;
			})
            ->addColumn('rombel', function ($item) {
				$nama_rombel = $item->nama.'/'.$item->tingkat;
				return $nama_rombel;
			})
            ->addColumn('jenis_kelamin', function ($item) {
				$return  = ($item->siswa) ? $item->siswa->jenis_kelamin : '-';
				return $return;
			})
			->addColumn('tgl_keluar', function ($item) {
				$return  = ($item->terhapus) ? HelperServiceProvider::TanggalIndo(date('Y-m-d', strtotime($item->terhapus))) : '-';
				return $return;
			})
            ->addColumn('actions', function ($item) {
				$links = '<div class="text-center"><a href="'.url('pd/view/'.$item->siswa_id).'" class="btn btn-success btn-sm toggle-modal"><i class="fa fa-eye"></i> Detil</a></a>';
                return $links;

            })
            ->rawColumns(['set_nama', 'set_tempat_lahir', 'agama', 'rombel', 'jenis_kelamin', 'tgl_keluar', 'actions'])
            ->make(true);  
	}
	public function view($siswa_id){
		/*$a = Siswa::find($siswa_id);
		$a->delete();
		return redirect()->route('pd_aktif');*/
		$data['siswa'] = Siswa::withTrashed()->find($siswa_id);
		$data['title'] = 'Detil Siswa';
		return view('siswa.view', $data);
	}
}
