<?php

namespace App;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
class Jurusan_sp extends Model
{
	use Uuid;
    public $incrementing = false;
	protected $table = 'jurusan_sp';
	protected $primaryKey = 'jurusan_sp_id';
	protected $fillable = [
        'jurusan_sp_id', 'jurusan_sp_id_dapodik', 'sekolah_id', 'jurusan_id', 'nama_jurusan_sp', 'last_sync',
    ];
}
