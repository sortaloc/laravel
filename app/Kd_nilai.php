<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
class Kd_nilai extends Model
{
    use Uuid;
	use SoftDeletes;
    public $incrementing = false;
	protected $table = 'kd_nilai';
	protected $primaryKey = 'kd_nilai_id';
	protected $guarded = [];
	public function kompetensi_dasar(){
		return $this->hasOne('App\Kompetensi_dasar', 'id', 'kd_id');
	}
	public function nilai(){
        return $this->hasMany('App\Nilai', 'kd_nilai_id', 'kd_nilai_id');
    }
}
