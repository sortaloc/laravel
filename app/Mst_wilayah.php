<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mst_wilayah extends Model
{
    public $incrementing = false;
	protected $table = 'mst_wilayah';
	protected $primaryKey = 'kode_wilayah';
	protected $fillable = [
        'kode_wilayah', 'nama', 'id_level_wilayah', 'mst_kode_wilayah', 'negara_id', 'asal_wilayah', 'kode_bps', 'kode_dagri', 'kode_keu', 'last_sync'
    ];
}
