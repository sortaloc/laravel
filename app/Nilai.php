<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
class Nilai extends Model
{
    use Uuid;
    use SoftDeletes;
    public $incrementing = false;
	protected $table = 'nilai';
	protected $primaryKey = 'nilai_id';
	protected $guarded = [];
	public function kd_nilai(){
        return $this->hasMany('App\Kd_nilai', 'rencana_penilaian_id', 'rencana_penilaian_id');
    }
	public function siswa(){
		//return $this->hasOne('App\Pembelajaran', 'pembelajaran_id', 'pembelajaran_id');
		return $this->hasOneThrough(
            /*'App\History',
            'App\User',
            'supplier_id', // Foreign key on users table...
            'user_id', // Foreign key on history table...
            'id', // Local key on suppliers table...
            'id' // Local key on users table...*/
			'App\Anggota_rombel',
            'App\Siswa',
            'siswa_id', // Foreign key on users table...
            'siswa_id', // Foreign key on history table...
            'anggota_rombel_id', // Local key on suppliers table...
            'siswa_id' // Local key on users table...
        );
	}
}
