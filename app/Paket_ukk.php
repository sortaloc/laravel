<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
class Paket_ukk extends Model
{
    use Uuid;
    use SoftDeletes;
    public $incrementing = false;
	protected $table = 'ref_paket_ukk';
	protected $primaryKey = 'paket_ukk_id';
	protected $guarded = [];
	public function jurusan(){
		return $this->hasOne('App\Jurusan', 'jurusan_id', 'jurusan_id');
	}
}
