<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use App\Nilai;
class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
	public static function prepare_send($str){
		return rawurlencode(base64_encode(gzcompress(self::encryptor(serialize($str)))));
	}
	public static function prepare_receive($str){
		return unserialize(self::decryptor(gzuncompress(base64_decode(rawurldecode($str)))));
	}
	public static function encryptor($str){
		return $str;
	}
	public static function decryptor($str){
		return $str;
	}
	public static function TanggalIndo($date){
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun; 
		return($result);
	}
	public static function jenis_gtk($query){
		$data['tendik'] = array(11,30,40,41,42,43,44,57,58,59);
		$data['guru'] = array(3,4,5,6,7,8,9,10,12,13,14,20,25,26,51,52,53,54,56);
		$data['instruktur'] = array(99);
		$data['asesor'] = array(98);
		return $data[$query];
	}
	public static function get_ta(){
		return DB::table('semester')->where('periode_aktif', 1)->first();
	}
	public static function table_sync(){
		$table_sync = array(
			1	=> 'absen',
			2	=> 'anggota_rombel',
			3	=> 'catatan_ppk',
			4	=> 'catatan_wali',
			5	=> 'deskripsi_mata_pelajaran',
			6	=> 'deskripsi_sikap',
			7	=> 'ekstrakurikuler',
			8	=> 'guru_terdaftar',
			9	=> 'jurusan_sp', //no semester
			10	=> 'kd_nilai', //no semester
			11	=> 'nilai',
			12	=> 'nilai_akhir',
			13	=> 'nilai_ekstrakurikuler',
			14	=> 'nilai_sikap',
			15	=> 'nilai_ukk',
			16	=> 'pembelajaran',
			17	=> 'prakerin',
			18	=> 'prestasi',
			19	=> 'ref_guru', //no semester
			20	=> 'ref_sekolah', //no semester
			21	=> 'ref_sikap', //no semester
			22	=> 'ref_siswa', //no semester
			23	=> 'remedial',
			24	=> 'rencana_penilaian',
			25	=> 'rombongan_belajar',
			26	=> 'teknik_penilaian', //no semester
			27	=> 'bobot_keterampilan',
			28	=> 'nilai_rapor',
			29	=> 'kenaikan_kelas',
			30	=> 'indikator_karakter',
			31 	=> 'ref_kompetensi_dasar', //no semester
			32 	=> 'semester',
		);
		return $table_sync;
	}
	public static function url_server_direktorat($action){
		return 'http://103.40.55.242/erapor_server/sync/'.$action;
	}
	public static function url_server_erapor($action){
		$url_server = 'http://103.40.55.226/dashboard_erapor/proses/'.$action;
		$url_server = 'http://localhost/dashboard_erapor/proses/'.$action;
		return $url_server;
	}
	public static function test($var){
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}
	public static function status_label($status){
		if($status == '1') : 
			$label = '<span class="btn btn-xs btn-success"> Aktif </span>';
		elseif ($status == '0') : 
			$label = '<span class="btn btn-xs btn-danger"> Non Aktif </span>';
		endif;
		return $label;
	}
	public static function check_2018(){
		$semester = self::get_ta();
		$tahun = substr($semester->semester_id,0,4);
		if($tahun >= 2018){
			return true;
		} else {
			return false;
		}
	}
	public static function get_kkm($kelompok_id, $kkm){
		$check_2018 = self::check_2018();
		if($check_2018){
			$produktif = array(4,5,9,10,13);
			$non_produktif = array(1,2,3,6,7,8,11,12,99);
			if(in_array($kelompok_id,$produktif)){
				$kkm = 65;
			} elseif(in_array($kelompok_id,$non_produktif)) {
				$kkm = 60;
			} else {
				$kkm = 0;
			}
		}
		return $kkm;
	}
	public static function get_nilai($anggota_rombel_id, $kd_nilai_id){
		$get_nilai = Nilai::where('anggota_rombel_id', '=', $anggota_rombel_id)->where('kd_nilai_id', '=', $kd_nilai_id)->first();
		$nilai = ($get_nilai) ? $get_nilai->nilai : '';
		return $nilai;
	}
	public static function array_to_object($d) {
        return is_array($d) ? (object) array_map(__METHOD__, $d) : $d;
    }
	public static function predikat($kkm, $nilai, $produktif = NULL){
		if($produktif){
			$result = array(
				'A+'	=> 100, // 95 - 100
				'A'		=> 94, // 90 - 94
				'A-'	=> 89, // 85 - 89
				'B+'	=> 84, // 80 - 84
				'B'		=> 79, // 75 - 79
				'B-'	=> 74, // 70 - 74
				'C'		=> 69, // 65 - 69
				'D'		=> 64, // 0 - 59
			);
		} else {
			$result = array(
				'A+'	=> 100, // 95 - 100
				'A'		=> 94, // 90 - 94
				'A-'	=> 89, // 85 - 89
				'B+'	=> 84, // 80 - 84
				'B'		=> 79, // 75 - 79
				'B-'	=> 74, // 70 - 74
				'C'		=> 69, // 60 - 69
				'D'		=> 59, // 0 - 59
			);
		}
		if($result[$nilai] > 100)
			$result[$nilai] = 100;
		return $result[$nilai];
	}
	public static function konversi_huruf($kkm, $nilai, $produktif = NULL){
		$check_2018 = self::check_2018();
		if($check_2018){
			$show = 'predikat';
			$a = self::predikat($kkm,'A') + 1;
			$a_min = self::predikat($kkm,'A-') + 1;
			$b_plus = self::predikat($kkm,'B+') + 1;
			$b = self::predikat($kkm,'B') + 1;
			$b_min = self::predikat($kkm,'B-') + 1;
			$c = self::predikat($kkm,'C') + 1;
			$d = self::predikat($kkm,'D', $produktif) + 1;
			if($nilai == 0){
				$predikat 	= '-';
			} elseif($nilai >= $a){//$settings->a_min){ //86
				$predikat 	= 'A+';
			} elseif($nilai >= $a_min){//$settings->a_min){ //86
				$predikat 	= 'A';
			} elseif($nilai >= $b_plus){//$settings->a_min){ //86
				$predikat 	= 'A-';
			} elseif($nilai >= $b){//$settings->a_min){ //86
				$predikat 	= 'B+';
			} elseif($nilai >= $b_min){//$settings->a_min){ //86
				$predikat 	= 'B';
			} elseif($nilai >= $c){//$settings->a_min){ //86
				$predikat 	= 'B-';
			} elseif($nilai >= $d){//$settings->a_min){ //86
				$predikat 	= 'C';
			} elseif($nilai < $d){//$settings->a_min){ //86
				$predikat 	= 'D';
			}
		} else {
			$b = self::predikat($kkm,'b') + 1;
			$c = self::predikat($kkm,'c') + 1;
			$d = self::predikat($kkm,'d') + 1;
			if($n == 0){
				$predikat 	= '-';
				$sikap		= '-';
				$sikap_full	= '-';
			} elseif($n >= $b){//$settings->a_min){ //86
				$predikat 	= 'A';
				$sikap		= 'SB';
				$sikap_full	= 'Sangat Baik';
			} elseif($n >= $c){ //71
				$predikat 	= 'B';
				$sikap		= 'B';
				$sikap_full	= 'Baik';
			} elseif($n >= $d){ //56
				$predikat 	= 'C';
				$sikap		= 'C';
				$sikap_full	= 'Cukup';
			} elseif($n < $d){ //56
				$predikat 	= 'D';
				$sikap		= 'K';
				$sikap_full	= 'Kurang';
			}
		}
		return $predikat;
	}
	public static function sebaran($input, $a,$b){
		$range_data = range($a,$b);	
		$output = array_intersect($input , $range_data);
		return $output;
	}
	public static function sebaran_tooltip($input, $a,$b,$c){
		$range_data = range($a,$b);
		$output = array_intersect($input , $range_data);
		$data = array();
		$nama_siswa = '';
		foreach($output as $k=>$v){
			$data[] = $k;
		}
		if(count($output) == 0){
			$result = count($output);
		} else {
			$result = '<a class="tooltip-'.$c.'" href="javascript:void(0)" title="'.implode('<br />',$data).'" data-html="true">'.count($output).'</a>';
		}
		return $result;
	}
}
