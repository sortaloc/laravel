<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rencana_penilaian extends Model
{
    use Uuid;
    use SoftDeletes;
    public $incrementing = false;
	protected $table = 'rencana_penilaian';
	protected $primaryKey = 'rencana_penilaian_id';
	protected $guarded = [];
	public function kd_nilai(){
        return $this->hasMany('App\Kd_nilai', 'rencana_penilaian_id', 'rencana_penilaian_id');
    }
	public function pembelajaran(){
		return $this->hasOne('App\Pembelajaran', 'pembelajaran_id', 'pembelajaran_id');
	}
	public function teknik_penilaian(){
		return $this->hasOne('App\Teknik_penilaian', 'teknik_penilaian_id', 'metode_id');
	}
}
