<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use App\Providers\HelperServiceProvider;
use Illuminate\Database\Eloquent\SoftDeletes;
class Siswa extends Model
{
    //use Uuid;
	use SoftDeletes;
    public $incrementing = false;
	protected $table = 'ref_siswa';
	protected $primaryKey = 'siswa_id';
	protected $guarded = [];
	protected static function boot(){
		parent::boot();
		static::creating(function ($model) {
            try {
                $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
		static::deleting(function($model) {
			foreach ($model->anggota_rombel()->get() as $model) {
				$model->delete();
			}
		});
    }
	public function anggota_rombel(){
		$semester = HelperServiceProvider::get_ta();
        return $this->hasMany('App\Anggota_rombel', 'siswa_id', 'siswa_id')->where('semester_id', '=', $semester->semester_id);
    }
	public function agama(){
		return $this->hasOne('App\Agama', 'id', 'agama_id');
	}
}
