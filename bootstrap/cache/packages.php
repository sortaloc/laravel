<?php return array (
  'beyondcode/laravel-dump-server' => 
  array (
    'providers' => 
    array (
      0 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    ),
  ),
  'chumper/zipper' => 
  array (
    'providers' => 
    array (
      0 => 'Chumper\\Zipper\\ZipperServiceProvider',
    ),
    'aliases' => 
    array (
      'Zipper' => 'Chumper\\Zipper\\Zipper',
    ),
  ),
  'codedge/laravel-selfupdater' => 
  array (
    'providers' => 
    array (
      0 => 'Codedge\\Updater\\UpdaterServiceProvider',
    ),
    'aliases' => 
    array (
      'Updater' => 'Codedge\\Updater\\UpdaterFacade',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'ixudra/curl' => 
  array (
    'providers' => 
    array (
      0 => 'Ixudra\\Curl\\CurlServiceProvider',
    ),
    'aliases' => 
    array (
      'Curl' => 'Ixudra\\Curl\\Facades\\Curl',
    ),
  ),
  'jeroennoten/laravel-adminlte' => 
  array (
    'providers' => 
    array (
      0 => 'JeroenNoten\\LaravelAdminLte\\ServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'maatwebsite/excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'santigarcor/laratrust' => 
  array (
    'providers' => 
    array (
      0 => 'Laratrust\\LaratrustServiceProvider',
    ),
    'aliases' => 
    array (
      'Laratrust' => 'Laratrust\\LaratrustFacade',
    ),
  ),
  'uxweb/sweet-alert' => 
  array (
    'providers' => 
    array (
      0 => 'UxWeb\\SweetAlert\\SweetAlertServiceProvider',
    ),
    'aliases' => 
    array (
      'Alert' => 'UxWeb\\SweetAlert\\SweetAlert',
    ),
  ),
  'yajra/laravel-datatables-oracle' => 
  array (
    'providers' => 
    array (
      0 => 'Yajra\\DataTables\\DataTablesServiceProvider',
    ),
    'aliases' => 
    array (
      'DataTables' => 'Yajra\\DataTables\\Facades\\DataTables',
    ),
  ),
);