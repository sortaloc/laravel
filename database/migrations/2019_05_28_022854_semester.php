<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Semester extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('semester', function (Blueprint $table) {
            $table->string('semester_id');
            $table->integer('tahun_ajaran_id');
			$table->string('nama');
			$table->integer('semester');
			$table->integer('periode_aktif')->default(0);
			$table->timestamps();
			$table->timestamp('last_sync');
            $table->primary('semester_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semester');
    }
}
