<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Guru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_guru', function (Blueprint $table) {
			$table->uuid('guru_id');
			$table->uuid('guru_id_dapodik')->nullable();
            $table->uuid('sekolah_id');
			$table->string('nama');
			$table->string('nuptk');
			$table->string('nip')->nullable();
			$table->string('jenis_kelamin');
			$table->string('tempat_lahir');
			$table->date('tanggal_lahir');
			$table->string('nik', 16)->nullable();
			$table->integer('jenis_ptk_id');
			$table->integer('agama_id');
			$table->string('alamat')->nullable();
			$table->string('rt')->nullable();
			$table->string('rw')->nullable();
			$table->string('desa_kelurahan')->nullable();
			$table->string('kecamatan')->nullable();
			$table->string('kode_pos')->nullable();
			$table->string('no_hp')->nullable();
			$table->string('email')->nullable();
			$table->string('photo')->nullable();
			$table->integer('guru_id_erapor')->nullable();
			$table->integer('is_dapodik')->default('0');
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
            $table->primary('guru_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_guru');
    }
}
