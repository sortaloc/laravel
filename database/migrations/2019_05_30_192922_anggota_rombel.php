<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnggotaRombel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('anggota_rombel')){
			Schema::create('anggota_rombel', function (Blueprint $table) {
				$table->uuid('anggota_rombel_id');
				$table->uuid('sekolah_id');
				$table->string('semester_id', 5);
				$table->uuid('rombongan_belajar_id');
				$table->uuid('siswa_id');
				$table->uuid('anggota_rombel_id_dapodik');
				$table->integer('anggota_rombel_id_erapor')->nullable();
				$table->timestamps();
				$table->softDeletes();
				$table->timestamp('last_sync');
				$table->foreign('sekolah_id')->references('sekolah_id')->on('ref_sekolah')
					->onUpdate('CASCADE')->onDelete('CASCADE');
				$table->foreign('siswa_id')->references('siswa_id')->on('ref_siswa')
					->onUpdate('CASCADE')->onDelete('CASCADE');
				$table->foreign('rombongan_belajar_id')->references('rombongan_belajar_id')->on('rombongan_belajar')
					->onUpdate('CASCADE')->onDelete('CASCADE');
				$table->foreign('semester_id')->references('semester_id')->on('semester')
					->onUpdate('CASCADE')->onDelete('CASCADE');
				$table->primary('anggota_rombel_id');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota_rombel');
    }
}
