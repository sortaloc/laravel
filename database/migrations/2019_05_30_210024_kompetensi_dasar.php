<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KompetensiDasar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kompetensi_dasar', function (Blueprint $table) {
			$table->increments('id');
			$table->string('id_kompetensi', 11);
			$table->string('aspek', 11);
			$table->integer('mata_pelajaran_id');
			$table->integer('kelas')->default('0');
            $table->string('id_kompetensi_nas', 11)->nullable();
			$table->text('kompetensi_dasar');
			$table->text('kompetensi_dasar_alias')->nullable();
			$table->uuid('user_id')->nullable();
			$table->integer('aktif')->default('1');
			$table->integer('kurikulum_id')->default('0')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->foreign('kurikulum_id')->references('kurikulum_id')->on('ref_kurikulum')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('mata_pelajaran_id')->references('mata_pelajaran_id')->on('mata_pelajaran')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_kompetensi_dasar');
    }
}
