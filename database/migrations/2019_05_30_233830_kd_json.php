<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KdJson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kd_json', function (Blueprint $table) {
			$table->uuid('kd_json_id');
			$table->string('nama_file', 100);
			$table->timestamps();
			$table->softDeletes();
			$table->primary('kd_json_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kd_json');
    }
}
