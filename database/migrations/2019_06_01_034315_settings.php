<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('settings', function (Blueprint $table) {
			$table->increments('id');
			$table->string('app_version');
			$table->string('db_version');
			$table->integer('zona')->default('1');
			$table->date('tanggal_rapor')->nullable();
			$table->timestamps();
			$table->timestamp('last_sync');
        });*/
		Schema::create('settings', function (Blueprint $table) {
			$table->string('key');
			$table->string('value');
			$table->primary('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
