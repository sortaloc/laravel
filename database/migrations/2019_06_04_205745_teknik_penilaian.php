<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeknikPenilaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teknik_penilaian', function (Blueprint $table) {
            $table->uuid('teknik_penilaian_id');
            $table->uuid('sekolah_id');
			$table->integer('kompetensi_id');
            $table->string('nama')->unique();
			$table->integer('bobot')->nullable();
			$table->integer('teknik_id_erapor')->nullable();
            $table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->foreign('sekolah_id')->references('sekolah_id')->on('ref_sekolah')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->primary('teknik_penilaian_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teknik_penilaian');
    }
}
