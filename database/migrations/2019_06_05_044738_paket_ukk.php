<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaketUkk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_paket_ukk', function (Blueprint $table) {
			$table->uuid('paket_ukk_id');
			$table->string('jurusan_id', 25);
			$table->integer('kurikulum_id');
			$table->integer('kode_kompetensi');
			$table->string('nomor_paket');
			$table->string('nama_paket');
			$table->integer('status')->default('1');
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->primary('paket_ukk_id');
			$table->foreign('jurusan_id')->references('jurusan_id')->on('ref_jurusan')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('kurikulum_id')->references('kurikulum_id')->on('ref_kurikulum')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
		Schema::create('ref_unit_ukk', function (Blueprint $table) {
			$table->uuid('unit_ukk_id');
			$table->uuid('paket_ukk_id');
			$table->string('kode_unit');
			$table->string('nama_unit');
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->primary('unit_ukk_id');
			$table->foreign('paket_ukk_id')->references('paket_ukk_id')->on('ref_paket_ukk')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
		Schema::create('rencana_ukk', function (Blueprint $table) {
			$table->uuid('rencana_ukk_id');
			$table->uuid('sekolah_id');
			$table->string('semester_id', 5);
			$table->uuid('paket_ukk_id');
			$table->uuid('internal');
			$table->uuid('eksternal');
			$table->string('nomor_sertifikat');
			$table->date('tanggal_sertifikat');
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->primary('rencana_ukk_id');
			$table->foreign('sekolah_id')->references('sekolah_id')->on('ref_sekolah')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('semester_id')->references('semester_id')->on('semester')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('paket_ukk_id')->references('paket_ukk_id')->on('ref_paket_ukk')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
		Schema::create('penilaian_ukk', function (Blueprint $table) {
			$table->uuid('penilaian_ukk_id');
			$table->uuid('sekolah_id');
			$table->string('semester_id', 5);
			$table->uuid('rencana_ukk_id');
			$table->uuid('siswa_id');
			$table->uuid('anggota_rombel_id');
			$table->integer('nilai');
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->primary('penilaian_ukk_id');
			$table->foreign('sekolah_id')->references('sekolah_id')->on('ref_sekolah')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('semester_id')->references('semester_id')->on('semester')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('rencana_ukk_id')->references('rencana_ukk_id')->on('rencana_ukk')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_paket_ukk');
		Schema::dropIfExists('ref_unit_ukk');
		Schema::dropIfExists('penilaian_ukk');
    }
}
