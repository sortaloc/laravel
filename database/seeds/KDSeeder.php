<?php

use Illuminate\Database\Seeder;
use Chumper\Zipper\Facades\Zipper;
use App\Kd_json;
use App\Kompetensi_dasar;
use Illuminate\Support\Facades\Storage;
class KDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->command->getOutput()->writeln("Memulai proses import ref. Kompetensi Dasar");
		$this->command->getOutput()->progressStart(0);
        if(!Storage::disk('public')->exists('storage/kd_json/0.json')){
			$Path = public_path('storage/kd_json.zip');
        	Zipper::make($Path)->extractTo('storage');
		}
		$all_data = Storage::files('public/kd_json');
		foreach($all_data as $data){
			$jumlah = Kompetensi_dasar::count();
			$this->command->getOutput()->progressAdvance();
			//$this->command->getOutput()->writeln($jumlah." KD berhasil di proses");
			$kd_kd_json = Kd_json::where('nama_file', str_replace('public/kd_json/', '', $data))->first();
			if($kd_kd_json){
				Kd_json::create(['nama_file' => str_replace('public/kd_json/', '', $data)]);
			}
			$json = Storage::disk('public')->get('kd_json/'.str_replace('public/kd_json/', '', $data));
			$response = json_decode($json);
			foreach($response->data as $obj){
				$create_kd = Kompetensi_dasar::updateOrCreate(
					['aspek' => $obj->aspek, 'mata_pelajaran_id' => $obj->mata_pelajaran_id, 'kompetensi_dasar' => $obj->kompetensi_dasar, 'kurikulum_id' => $obj->kurikulum_id, 'kelas' => $obj->kelas],
					['id_kompetensi' => $obj->id_kompetensi, 'id_kompetensi_nas' => $obj->id_kompetensi_nas, 'kompetensi_dasar_alias' => $obj->kompetensi_dasar_alias, 'aktif' => $obj->aktif, 'created_at' => $obj->created_at, 'updated_at' => $obj->updated_at, 'deleted_at' => $obj->deleted_at, 'last_sync' => $obj->last_sync]
				);
			}
		}
		$this->command->getOutput()->progressFinish();
		$this->command->getOutput()->writeln("Proses import ref. Kompetensi Dasar selesai");
    }
}
