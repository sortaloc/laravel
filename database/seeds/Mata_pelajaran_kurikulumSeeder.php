<?php

use Illuminate\Database\Seeder;

class Mata_pelajaran_kurikulumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Mata_pelajaran_kurikulum::query()->truncate();
		$limit = 10000;
		for($i = 0; $i <= 51353; $i += $limit){
			$json = File::get('database/data/mata_pelajaran_kurikulum_'.$i.'.json');
			$data = json_decode($json);
			foreach($data as $obj){
				DB::table('mata_pelajaran_kurikulum')->insert([
					'kurikulum_id' 			=> $obj->kurikulum_id,
					'mata_pelajaran_id'		=> $obj->mata_pelajaran_id,
					'tingkat_pendidikan_id'	=> $obj->tingkat_pendidikan_id,
					'jumlah_jam'			=> $obj->jumlah_jam,
					'jumlah_jam_maksimum'	=> $obj->jumlah_jam_maksimum,
					'wajib'					=> $obj->wajib,
					'sks'					=> $obj->sks,
					'a_peminatan'			=> $obj->a_peminatan,
					'area_kompetensi'		=> $obj->area_kompetensi,
					'gmp_id'				=> $obj->gmp_id,
					'created_at' 			=> $obj->created_at,
					'updated_at' 			=> $obj->updated_at,
					'deleted_at'			=> $obj->deleted_at,
					'last_sync'				=> $obj->last_sync,
				]);
			}
		}
    }
}
