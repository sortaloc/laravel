<?php

use Illuminate\Database\Seeder;

class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ref_semester = array(
			array(
				'semester_id' 		=> '20171',
				'tahun_ajaran_id'	=> 2017,
				'nama' 				=> '2017/2018',
				'semester' 			=> 1,
				'periode_aktif'		=> 0
			),
			array(
				'semester_id' 		=> '20172',
				'tahun_ajaran_id'	=> 2017,
				'nama' 				=> '2017/2018',
				'semester' 			=> 2,
				'periode_aktif'		=> 0
			),
			array(
				'semester_id' 		=> '20181',
				'tahun_ajaran_id'	=> 2018,
				'nama' 				=> '2018/2019',
				'semester' 			=> 1,
				'periode_aktif'		=> 0
			),
			array(
				'semester_id' 		=> '20182',
				'tahun_ajaran_id'	=> 2018,
				'nama' 				=> '2018/2019',
				'semester' 			=> 2,
				'periode_aktif'		=> 1
			),
		);
		DB::table('semester')->truncate();
		foreach($ref_semester as $semester){
    		DB::table('semester')->insert([
    			'semester_id' 		=> $semester['semester_id'],
    			'tahun_ajaran_id' 	=> $semester['tahun_ajaran_id'],
    			'nama' 				=> $semester['nama'],
				'semester'			=> $semester['semester'],
				'periode_aktif'		=> $semester['periode_aktif'],
				'created_at' 		=> date('Y-m-d H:i:s'),
				'updated_at' 		=> date('Y-m-d H:i:s'),
				'last_sync'			=> date('Y-m-d H:i:s'),
    		]);
 
    	}
    }
}
