@extends('adminlte::page')

@section('content_header')
    <h1>Beranda</h1>
@stop
@section('box-title')
Selamat Datang {{ $user->name }}
@stop
@section('content')
	@role('admin')
		<div class="row">
			<div class="col-lg-3 col-sm-3 col-xs-6">
				<div class="small-box bg-green disabled color-palette">
					<div class="inner">
						<h3>{{$guru}}</h3>
						<p>PTK</p>
					</div>
				<div class="icon"><i class="ion ion-person-add"></i></div>
					<a href="{{url('guru')}}" class="small-box-footer">
						Selengkapnya <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-sm-3 col-xs-6">
				<div class="small-box bg-yellow disabled color-palette">
					<div class="inner">
						<h3>{{$siswa}}</h3>
						<p>Peserta Didik</p>
					</div>
					<div class="icon"><i class="ion ion-android-contacts"></i></div>
						<a href="{{url('pd-aktif')}}" class="small-box-footer">
							Selengkapnya <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div><!-- ./col -->
				<div class="col-lg-3 col-sm-3 col-xs-6">
					<div class="small-box bg-red disabled color-palette">
						<div class="inner">
							<h3>0</h3>
							<p>Rencana Penilaian (P&amp;K)</p>
						</div>
						<div class="icon"><i class="ion ion-android-checkbox-outline"></i></div>
						<a href="javascript:void(0)" class="small-box-footer">&nbsp;</a>
					</div>
				</div><!-- ./col -->
				<div class="col-lg-3 col-sm-3 col-xs-6">
					<div class="small-box bg-maroon disabled color-palette">
						<div class="inner">
							<h3>0</h3>
							<p>Penilaian Per KD (P&amp;K)</p>
						</div>
						<div class="icon"><i class="ion ion-arrow-graph-up-right"></i></div>
						<a href="javascript:void(0)" class="small-box-footer">&nbsp;</a>
					</div>
				</div><!-- ./col -->
			</div>
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<div class="box-header with-border">
						<h3 class="box-title"><strong>Identitas Sekolah</strong></h3>
					</div>
					<table class="table table-condensed">
						<tr>
							<td width="30%">Nama Sekolah</td>
							<td width="70%">: {{$sekolah->nama}}</td>
						</tr>
					<tr>
						<td>NPSN</td>
						<td>: {{$sekolah->npsn}}</td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>: {{$sekolah->alamat}}</td>
					</tr>
					<tr>
						<td>Kodepos</td>
						<td>: {{$sekolah->kode_pos}}</td>
					</tr>
					<tr>
						<td>Desa/Kelurahan</td>
						<td>: {{$sekolah->desa_kelurahan}}</td>
					</tr>
					<tr>
						<td>Kecamatan</td>
						<td>: {{$sekolah->kecamatan}}</td>
					</tr>
					<tr>
						<td>Kabupaten/Kota</td>
						<td>: {{$sekolah->kabupaten}}</td>
					</tr>
					<tr>
						<td>Provinsi</td>
						<td>: {{$sekolah->provinsi}}</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>: {{$sekolah->email}}</td>
					</tr>
					<tr>
						<td>Website</td>
						<td>: {{$sekolah->website}}</td>
					</tr>
					<tr>
					<?php
					$data_guru = App\Guru::find($sekolah->guru_id);
					?>
						<td>Kepala Sekolah</td>
						<td>: {{ ($data_guru) ? $data_guru->nama : '-' }}</td>
					</tr>
				</table>
			</div>
			<div class="col-lg-6 col-xs-12">
				<div class="box-header with-border">
					<h3 class="box-title"><strong>Informasi Aplikasi</strong></h3>
				</div>
						<table class="table table-condensed">
					<tr>
						<td width="30%">Nama Aplikasi</td>
						<td width="70%">: {{config('site.app_name')}}</td>
					</tr>
					<tr>
						<td>Versi Aplikasi</td>
						<td>: {{config('site.app_version')}}</td>
					</tr>
					<tr>
						<td>Versi Database</td>
						<td>: {{config('site.db_version')}}</td>
					</tr>
					<tr>
						<td>Group Diskusi</td>
						<td>: <a href="https://www.facebook.com/groups/2003597939918600/" target="_blank" class="btn btn-sm btn-social btn-facebook"><i class="fa fa-facebook"></i>FB Group</a> <a href="http://t.me/eRaporSMK" target="_blank" class="btn btn-sm btn-social btn-info"><i class="fa fa-paper-plane"></i>Telegram</a></td>
					</tr>
					<tr>
						<td>Tim Helpdesk</td>
						<td>
							<a class="btn btn-sm btn-block btn-social btn-success" href="https://api.whatsapp.com/send?phone=628156441864&text=NPSN%3A%2020613916"><i class="fa fa-whatsapp"></i> Wahyudin [08156441864]</a>
							<a class="btn btn-sm btn-block btn-social btn-success" href="https://api.whatsapp.com/send?phone=6281229997730&amp;text=NPSN%3A%2020613916"><i class="fa fa-whatsapp"></i> Ahmad Aripin [081229997730]</a>
							<a class="btn btn-sm btn-block btn-social btn-success" href="https://api.whatsapp.com/send?phone=6282113057512&amp;text=NPSN%3A%2020613916"><i class="fa fa-whatsapp"></i> Iman [082113057512]</a>
							<a class="btn btn-sm btn-block btn-social btn-success" href="https://api.whatsapp.com/send?phone=6282174508706&amp;text=NPSN%3A%2020613916"><i class="fa fa-whatsapp"></i> Ikhsan [082174508706]</a>
							<a class="btn btn-sm btn-block btn-social btn-success" href="https://api.whatsapp.com/send?phone=6282134924288&amp;text=NPSN%3A%2020613916"><i class="fa fa-whatsapp"></i> Toni [082134924288]</a>
							<a class="btn btn-sm btn-block btn-social btn-success" href="https://api.whatsapp.com/send?phone=628174144627&amp;text=NPSN%3A%2020613916"><i class="fa fa-whatsapp"></i> Hendri [08174144627]</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	@endrole
	@role('tu')
		<p>This is visible to users with the admin role. Gets translated to \Laratrust::hasRole('tu')</p>
	@endrole
	@role('guru')
		<h4 class="page-header">Mata Pelajaran yang diampu di Tahun Pelajaran {{$semester->nama}} Semester {{($semester->semester == 1) ? 'Ganjil' : 'Genap'}}</h4>
		<table class="table table-bordered table-striped table-hover datatable">
			<thead>
				<tr>
					<th rowspan="2"  style="width: 10px;vertical-align:middle;" class="text-center">#</th>
					<th rowspan="2" style="vertical-align:middle;" class="text-center">Mata Pelajaran</th>
					<th rowspan="2" style="vertical-align:middle;" class="text-center">Rombel</th>
					<th rowspan="2" style="vertical-align:middle;" class="text-center">Wali Kelas</th>
					<th rowspan="2" style="vertical-align:middle;" class="text-center">Jumlah PD</th>
					<th class="text-center" colspan="2">Generate Nilai</th>
				</tr>
				<tr>
					<th class="text-center">Pengetahuan</th>
					<th class="text-center">Keterampilan</th>
				</tr>
			</thead>
			<tbody>
		@if($all_pembelajaran->count())
			@foreach($all_pembelajaran as $pembelajaran)
				{{--dd($pembelajaran)--}}
				<?php
				if($pembelajaran->nilai_akhir_pengetahuan_count){
					$class_p = 'danger';
					$text_p = 'Perbaharui';
				} else {
					$text_p = 'Proses';
					$class_p = 'success';
				}
				if($pembelajaran->nilai_akhir_keterampilan_count){
					$text_k = 'Perbaharui';
					$class_k = 'danger';
				} else {
					$text_k = 'Proses';
					$class_k = 'success';
				}
				?>
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$pembelajaran->mata_pelajaran->nama}} ({{$pembelajaran->mata_pelajaran->mata_pelajaran_id}})</td>
					<td class="text-center">{{$pembelajaran->rombongan_belajar->nama}}</td>
					<td>{{$pembelajaran->rombongan_belajar->wali->nama}}</td>
					<td class="text-center">{{$pembelajaran->rombongan_belajar->anggota_rombel_count}}</td>
					<td><div class="text-center"><?php echo ($pembelajaran->rencana_pengetahuan_count) ? '<a href="'.url('/generate-nilai/'.$pembelajaran->pembelajaran_id.'/1').'" class="generate_nilai btn btn-sm btn-'.$class_p.' btn_generate btn-sm"><i class="fa fa-check-square-o"></i> '.$text_p.'</a>' : '-'; ?></div></td>
					<td><div class="text-center"><?php echo ($pembelajaran->rencana_keterampilan_count) ? '<a href="'.url('/generate-nilai/'.$pembelajaran->pembelajaran_id.'/2').'" class="generate_nilai btn btn-sm btn-'.$class_k.' btn_generate btn-sm"><i class="fa fa-check-square-o"></i> '.$text_k.'</a>' : '-'; ?></div></td>
				</tr>
			@endforeach
		@else
			<tr><td class="text-center" colspan="7">Anda tidak memiliki jadwal mengajar!</td></tr>
		@endif
			</tbody>
		</table>
		{{--CustomHelper::test($all_pembelajaran)--}}
	@endrole
	@role('wali')
	@if ($message = Session::get('success'))
      <div class="alert alert-success alert-block alert-dismissable"><i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Sukses!</strong> {{ $message }}
      </div>
    @endif

    @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block alert-dismissable"><i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> {{ $message }}
      </div>
    @endif
	<div class="col-lg-12 col-xs-12">
		<section id="mata-pelajaran">
		<?php 
		$rombongan_belajar_id = $rombongan_belajar->rombongan_belajar_id;
		$aktifkan = ($rombongan_belajar->kunci_nilai) ? 1 : 0;
		?>
			<h4>Anda adalah Wali Kelas Rombongan Belajar <label class="label bg-green">{{$rombongan_belajar->nama}}</label></h4>
			<h5>Daftar Mata Pelajaran di Rombongan Belajar <label class="label bg-green">{{$rombongan_belajar->nama}}</label></h5>
			<p>Status Penilaian di Rombongan Belajar ini : <span class="btn btn-xs btn-{{($rombongan_belajar->kunci_nilai) ? 'danger' : 'success'}}"> {{($rombongan_belajar->kunci_nilai) ? 'Non Aktif' : 'Aktif'}} </span> <a class="btn btn-{{($rombongan_belajar->kunci_nilai) ? 'success' : 'danger'}} btn-xs" href="{{url('/kunci-nilai/'.$rombongan_belajar_id.'/'.$aktifkan)}}"><i class="fa fa-power-off"></i> {{($rombongan_belajar->kunci_nilai == 1) ? 'Aktifkan' : 'Non Aktifkan'}}</a></p>
			<div class="row">
				<div class="col-lg-12 col-xs-12" style="margin-bottom:20px;">
					<table class="table table-bordered table-striped table-hover datatable">
						<thead>
							<tr>
								<th style="width: 10px; vertical-align:middle;" class="text-center" rowspan="2">#</th>
								<th rowspan="2" style="vertical-align:middle;">Mata Pelajaran</th>
								<th rowspan="2" style="vertical-align:middle;">Guru Mata Pelajaran</th>
								<th class="text-center" rowspan="2" style="vertical-align:middle;">SKM</th>
								<th class="text-center" colspan="2">Jumlah Rencana Penilaian</th>
								<th class="text-center" colspan="2">Generate Nilai</th>
							</tr>
							<tr>
								<th class="text-center">Pengetahuan</th>
								<th class="text-center">Keterampilan</th>
								<th class="text-center">Pengetahuan</th>
								<th class="text-center">Keterampilan</th>
							</tr>
						</thead>
						<tbody>
							@if($rombongan_belajar->pembelajaran->count())
							@foreach($rombongan_belajar->pembelajaran as $pembelajaran)
							<?php
							if($pembelajaran->nilai_akhir_pengetahuan_count){
								$class_p = 'danger';
								$text_p = 'Perbaharui';
							} else {
								$text_p = 'Proses';
								$class_p = 'success';
							}
							if($pembelajaran->nilai_akhir_keterampilan_count){
								$text_k = 'Perbaharui';
								$class_k = 'danger';
							} else {
								$text_k = 'Proses';
								$class_k = 'success';
							}
							?>
							<tr>
								<td class="text-center">{{$loop->iteration}}</td> 
								<td>{{$pembelajaran->nama_mata_pelajaran}} ({{$pembelajaran->mata_pelajaran_id}})</td>
								<td>{{($pembelajaran->pengajar) ? $pembelajaran->pengajar->nama : ($pembelajaran->guru) ? $pembelajaran->guru->nama : '-'}}</td>
								<td class="text-center">0</td>
								<td class="text-center">{{$pembelajaran->rencana_pengetahuan_count}}</td>
								<td class="text-center">{{$pembelajaran->rencana_keterampilan_count}}</td>
								<td><div class="text-center"><?php echo ($pembelajaran->rencana_pengetahuan_count) ? '<a href="'.url('/generate-nilai/'.$pembelajaran->pembelajaran_id.'/1').'" class="generate_nilai btn btn-sm btn-'.$class_p.' btn_generate btn-sm"><i class="fa fa-check-square-o"></i> '.$text_p.'</a>' : '-'; ?></div></td>
								<td><div class="text-center"><?php echo ($pembelajaran->rencana_keterampilan_count) ? '<a href="'.url('/generate-nilai/'.$pembelajaran->pembelajaran_id.'/2').'" class="generate_nilai btn btn-sm btn-'.$class_k.' btn_generate btn-sm"><i class="fa fa-check-square-o"></i> '.$text_k.'</a>' : '-'; ?></div></td>
							</tr>
							@endforeach
							@else
							<tr>
								<td class="text-center" colspan="8">Tidak ada data untuk ditampilkan</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	@endrole
	@role('siswa')
		<p>This is visible to users with the admin role. Gets translated to \Laratrust::hasRole('siswa')</p>
	@endrole
	@role('user')
		<p>This is visible to users with the admin role. Gets translated to \Laratrust::hasRole('user')</p>
	@endrole
	@role('waka')
		<p>This is visible to users with the admin role. Gets translated to \Laratrust::hasRole('waka')</p>
	@endrole
	@role('kaprog')
		<p>This is visible to users with the admin role. Gets translated to \Laratrust::hasRole('kaprog')</p>
	@endrole
	<h5>Aplikasi <strong>{{config('site.app_name')}}</strong> ini dibuat dan dikembangkan oleh Direktorat Pembinaan Sekolah Menengah Kejuruan</h5>
	<h5>Kementerian Pendidikan dan Kebudayaan Republik Indonesia</h5>
@stop
@section('js')
<script type="text/javascript">
$('a.generate_nilai').bind('click',function(e) {
	e.preventDefault();
	var url = $(this).attr('href');
	var ini = $(this);
	var myhtml = document.createElement("div");
 	myhtml.innerHTML = "Generate nilai ini akan mengeksekusi perubahan nilai.<br />Setelah generate akan dikunci kembali";
	swal({
		title: "Anda Yakin?",
		content: myhtml,
		icon: "warning",
		buttons: true,
		dangerMode: true,
		closeOnClickOutside: false,
	}).then((willDelete) => {
		if (willDelete) {
			$.get(url).done(function(response) {
				var data = $.parseJSON(response);
				swal({title: data.title, text: data.text,icon: data.icon, closeOnClickOutside: false}).then((result) => {
					window.location.replace('<?php echo url('/home'); ?>');
				});
			});
		}
	});
});
</script>
@stop