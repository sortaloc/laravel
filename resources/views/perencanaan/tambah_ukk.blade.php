@extends('adminlte::page')

@section('content_header')
    <h1>Tambah Data Perencanaan Penilaian UKK</h1>
@stop

@section('content')
    <form action="{{ route('simpan_ukk') }}" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control" type="text" name="name" value="{{ old('name') }}">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input class="form-control" type="text" name="description" value="{{ old('description') }}">
		</div>
		<div class="form-group">
			<input class="btn btn-primary" type="submit" value="Proses">
		</div>
	</form>
@stop