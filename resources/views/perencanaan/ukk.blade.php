@extends('adminlte::page')

@section('content_header')
    <h1>Perencanaan Penilaian UKK</h1>
@stop
@section('content_header_right')
<a href="{{url('perencanaan/tambah-ukk')}}" class="btn btn-success pull-right">Tambah Data</a>
@stop
@section('content')
	@if ($message = Session::get('success'))
      <div class="alert alert-success alert-block alert-dismissable"><i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Sukses!</strong> {{ $message }}
      </div>
    @endif

    @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block alert-dismissable"><i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> {{ $message }}
      </div>
    @endif
    <p>You are logged in!</p>
@stop