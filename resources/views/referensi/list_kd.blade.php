@extends('adminlte::page')

@section('title_postfix', ' | Referensi Kompetensi Dasar')

@section('content_header')
    <h1>Referensi Kompetensi Dasar</h1>
@stop

<?php
/*
@section('box-title')
	Judul
@stop
*/
?>
@section('content')
	<div class="row" style="margin-bottom:10px;">
		<input type="hidden" id="filter_tingkat" value="0" />
		<input type="hidden" id="filter_jurusan" value="0" />
		<div class="col-md-6">
			<select id="filter_mapel" class="form-control select2" style="width:100%;">
				<option value="">==Filter Berdasar Mata Pelajaran==</option>
				@if($all_pembelajaran->count())
				@foreach($all_pembelajaran as $pembelajaran)
				<option value="{{$pembelajaran->mata_pelajaran_id}}">{{$pembelajaran->nama_mata_pelajaran}}</option>
				@endforeach
				@endif
			</select>
		</div>
		<div id="filter_kelas_show" class="col-md-6" style="display:none;">
			<select id="filter_kelas" class="form-control select2" style="display:none;width:100%">
				<option value="">==Filter Berdasar Tingkat Kelas==</option>
				<option value="10">Kelas 10</option>
				<option value="11">Kelas 11</option>
				<option value="12">Kelas 12</option>
				<option value="13">Kelas 13</option>
			</select>
		</div>
	</div>
	<table id="datatable" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th style="width: 15%">Mata Pelajaran</th>
				<th style="width: 5%" class="text-center">Kode</th>
                <th style="width: 5%" class="text-center">Kelas</th>
				<th style="width: 20%">Isi Kompetensi</th>
                <th style="width: 20%">Ringkasan Kompetensi</th>
				<th style="width: 5%" colspan="text-center">Status</th>
                <th style="width: 5%" class="text-center">Tindakan</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
<div id="modal_content" class="modal fade"></div>
@stop

@section('js')
<script type="text/javascript">
var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
var bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
function turn_on_icheck(){
	$('a.toggle-modal').bind('click',function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$('#modal_content').modal('open');
	        $('.editor').wysihtml5();
		} else {
			$.get(url, function(data) {
				$('#modal_content').modal();
				$('#modal_content').html(data);
			});
		}
	});
}
$(document).ready( function () {
	$('.select2').select2();
	var oTable = $('#datatable').DataTable( {
		"retrieve": true,
		"processing": true,
        "serverSide": true,
        "ajax": {
			"url": "{{ url('referensi/list-kd') }}",
			"data": function (d) {
				var mata_pelajaran_id = $('#filter_mapel').val();
				var filter_kelas = $('#filter_kelas').val();
				if(mata_pelajaran_id){
					d.mata_pelajaran_id = mata_pelajaran_id;
				}
				if(filter_kelas){
					d.filter_kelas = filter_kelas;
				}
			}
		},
		"columns": [
            { "data": "mata_pelajaran.nama" },
            { "data": "id_kompetensi" },
			{ "data": "kelas" },
			{ "data": "isi_kd" },
			{ "data": "kompetensi_dasar_alias" },
			{ "data": "status" },
			{ "data": "tindakan" },
        ],
		"fnDrawCallback": function(oSettings){
			turn_on_icheck();
		}
    });
	$('#filter_mapel').change(function(e){
		var ini = $(this).val();
		if(ini == ''){
			$('#filter_kelas_show').hide();
		} else {
			$('#filter_kelas_show').show();
			$('#filter_kelas').prop("selectedIndex", 0);
			$("#filter_kelas").trigger('change.select2');
		}
        oTable.draw();
        e.preventDefault();
    });
	$('#filter_kelas').change(function(e){
        oTable.draw();
        e.preventDefault();
    });
});
</script>
@Stop