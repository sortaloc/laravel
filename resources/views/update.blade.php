@extends('adminlte::page')

@section('content_header')
    <h1>Cek Pembaharuan</h1>
@stop

@section('content')
	{{--dd(config('self-update.http.repository_url'))--}} 
	<?php
	if ($updater) {
		$update = 'success';
		$title = 'Pembaharuan Tersedia';
		$status = 'Gunakan Tombol di bawah ini untuk memperbaharui aplikasi';
		$tombol  = '<a style="text-decoration:none;" class="btn btn-lg btn-warning" id="check_update">Proses Pembaharuan</a>';
	} else {
		$update = 'danger';
		$title = 'Status Pembaharuan Aplikasi';
		$status = 'Belum tersedia pembaharuan untuk versi aplikasi Anda';
		$tombol  = '';
	}
	?>
	<div class="callout callout-<?php echo $update; ?>">
		<h4><?php echo $title; ?></h4>
		<p><?php echo $status; ?></p>
		<p><?php echo $tombol; ?></p>
	</div>
	<table class="table table-bordered" id="result" style="display:none;">
		<tr>
			<td>Mengunduh File Updater</td>
			<td><span class="download"><p class="text-yellow"><strong>[PROSES]</strong></p></span></td>
		</tr>
		<tr>
			<td>Mengekstrak File Updater</td>
			<td><span class="extract_to"><p class="text-yellow"><strong>[PROSES]</strong></p></span></td>
		</tr>
		<tr>
			<td>Memproses Pembaharuan</td>
			<td><span class="update_versi"><p class="text-yellow"><strong>[PROSES]</strong></p></span></td>
		</tr>
	</table>
	<div id="updater"></div>
	<a class="btn btn-success" id="sukses" href="<?php echo url()->current();?>" style="display:none;">Muat Ulang Aplikasi</a>
@stop


@section('js')
<script type="text/javascript">
$('#check_update').click(function(){
	$('#result').show();
	$.ajax({
		url: '<?php echo url('proses-update');?>',
		type: 'get',
		success: function(response){
			var data = $.parseJSON(response);
			$('.download').html(data.text);
			if(data.md5_file_local !== data.md5_file_server){
				swal({
					title:'Gagal',
					icon:'error',
					content:'Gagal mengunduh file updater. Silahkan coba lagi!',
					button:'Muat Ulang Aplikasi',
					closeOnClickOutside: false,
				}).then((value) => {
					window.location.replace('<?php echo url()->current(); ?>');
				});
				return false;
			}
			$.ajax({
				url: '<?php echo url('ekstrak');?>',
				type: 'get',
				success: function(response){
					var data = $.parseJSON(response);
					$('.extract_to').html(data.text);
					if(data.response === 0){
						swal({
							title:'Gagal',
							icon:'error',
							content:'Gagal Mengekstrak File Updater. Silahkan coba lagi!',
							button:'Muat Ulang Aplikasi',
							closeOnClickOutside: false,
						}).then((value) => {
							window.location.replace('<?php echo url()->current(); ?>');
						});
						return false;
					}
					$.ajax({
						url: '<?php echo url('update-versi');?>',
						type: 'get',
						success: function(response){
							console.log(response);
							var data = $.parseJSON(response);
							$('.update_versi').html(data.text);
							if(data.response === 0){
								swal({
									title:'Gagal',
									icon:'error',
									content:'Gagal Memproses Pembaharuan. Silahkan coba lagi!',
									button:'Muat Ulang Aplikasi',
									closeOnClickOutside: false,
								}).then((value) => {
									window.location.replace('<?php echo url()->current(); ?>');
								});
								return false;
							}
							window.setTimeout(function() {
								swal({
									title:'Sukses',
									icon:'success',
									content:'Berhasil memperbarui aplikasi',
									button:'Muat Ulang Aplikasi',
									closeOnClickOutside: false,
								}).then((value) => {
									window.location.replace('<?php echo url()->current(); ?>');
								});
							}, 1000);
						}
					});
				}
			});
		}
	});
})
</script>
@stop