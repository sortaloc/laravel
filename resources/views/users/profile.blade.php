@extends('adminlte::page')

@section('content_header')
    <h1>{{$title}}</h1>
@stop
@section('content_header_right')
    <?php echo $content_header_right; ?>
@stop

@section('content')
	{{-- dd($errors) --}}
	@if ($message = Session::get('success'))
      <div class="alert alert-success alert-block alert-dismissable"><i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Sukses!</strong> {{ $message }}
      </div>
    @endif

    @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block alert-dismissable"><i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> {{ $message }}
      </div>
    @endif
<form id="form-change-password" role="form" method="POST" action="{{ route('user.update_profile', ['id' => $user->user_id]) }}" novalidate enctype="multipart/form-data">
	<div class="col-md-6">
		<label for="name" class="col-form-label">Nama</label>
		<div class="form-group">
			<input type="text" value="{{$user->name}}" id="name" name="name" class="form-control">
		</div>
		<label for="email" class="col-form-label">Email</label>
		<div class="form-group">
			<input type="email" value="{{$user->email}}" id="email" name="email" class="form-control">
		</div>
		<label for="current-password" class="col-form-label">Password Saat Ini (Biarkan kosong jika tidak ingin merubah)</label>
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
			<input type="password" class="form-control" id="current-password" name="current_password" placeholder="Password">
		</div>
		<label for="password" class="col-form-label">Password Baru</label>
		<div class="form-group">
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
		</div>
		<label for="password_confirmation" class="col-form-label">Konfirmasi Password</label>
		<div class="form-group">
				<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">
		</div>
	</div>
	<div class="col-md-6">
		<label for="current-password" class="col-form-label">Foto Profil</label>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<?php $img = ($user->photo!= '')  ? url('storage/images/245/'.$user->photo) : url('vendor/img/avatar3.png'); ?><img src="<?php echo $img;?>" class="user-image" alt="User Image" />
					<input type="file" name="image">
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-danger">Update</button>
		</div>
	</div>
</form>
	
@endsection