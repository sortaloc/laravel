<?php
$a = url('/');
$a = parse_url($a);
$semester = DB::table('semester')->where('periode_aktif', 1)->first();
//dd($semester);
?>
@if (is_string($item))
    <li class="header">{{ str_replace('periode','Periode Aktif: '.$semester->nama.' SMT '.$semester->semester,$item) }}</li>
@else
    <li class="{{ $item['class'] }}">
        <a href="{{ $item['href'] }}"
           @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
        >
            <i class="fa fa-fw fa-{{ isset($item['icon']) ? $item['icon'] : 'circle-o' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i>
            <span>{{ $item['text'] }}</span>
            @if (isset($item['label']))
                <span class="pull-right-container">
                    <span class="label label-{{ isset($item['label_color']) ? $item['label_color'] : 'primary' }} pull-right">{{ $item['label'] }}</span>
                </span>
            @elseif (isset($item['submenu']))
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            @endif
        </a>
        @if (isset($item['submenu']))
            <ul class="{{ $item['submenu_class'] }}">
                {{-- @each('adminlte::partials.menu-item', $item['submenu'], 'item') --}}
				@foreach($item['submenu'] as $item)
					{{-- @if($a['host'] == 'localhost' && $item['url'] == 'kirim_nilai') --}}
						@include('adminlte::partials.menu-item', $item)
					{{-- @endif --}}
				@endforeach
            </ul>
        @endif
    </li>
@endif
