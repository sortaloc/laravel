<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('/', function () {
    return view('home');
});
*/



Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UsersController@index')->name('users');
Route::get('/users/list_user', 'UsersController@list_user')->name('list_user');
Route::get('/users/edit/{id}', ['as' => 'users.edit', 'uses' => 'UsersController@edit']);
Route::put('/users/update/{id}', array('as' => 'user.update', 'uses' => 'UsersController@update'));
Route::get('/users/profile', array('as' => 'user.profile', 'uses' => 'UsersController@profile'));
Route::get('/users/reset-password/{id}', ['as' => 'user.reset_password', 'uses' => 'UsersController@reset_password']);
Route::post('/users/update_profile/{id}', array('as' => 'user.update_profile', 'uses' => 'UsersController@update_profile'));
Route::get('/role', 'RolesController@index')->name('role');
Route::get('/role/list_role', 'UsersController@list_role')->name('list_role');
Route::get('/role_index', array('as' => 'roles.index', 'uses' => 'RolesController@index'));
Route::post('/role_store', array('as' => 'roles.store', 'uses' => 'RolesController@store'));
Route::get('/role_create', array('as' => 'roles.create', 'uses' => 'RolesController@create'));
Route::get('/role_edit/{id}', ['as' => 'roles.edit', 'uses' => 'RolesController@edit']);
Route::put('/role_update/{id}', ['as' => 'roles.update', 'uses' => 'RolesController@update']);
Route::get('/role_show', array('as' => 'roles.show', 'uses' => 'RolesController@show'));
Route::get('/permission', array('as' => 'permission.index', 'uses' => 'PermissionController@index'));
Route::get('/permission_create', array('as' => 'permission.create', 'uses' => 'PermissionController@create'));
Route::post('/permission_store', array('as' => 'permission.store', 'uses' => 'PermissionController@store'));
Route::get('/permission_edit/{id}', ['as' => 'permission.edit', 'uses' => 'PermissionController@edit']);
Route::get('/permission_show/{id}', array('as' => 'permission.show', 'uses' => 'PermissionController@show'));
Route::put('/permission_update/{id}', array('as' => 'permission.update', 'uses' => 'PermissionController@update'));
Route::delete('/permission_delete/{id}', array('as' => 'permission.destroy', 'uses' => 'PermissionController@destroy'));
Route::get('/pengguna', array('as' => 'users.index', 'uses' => 'PenggunaController@index'));
Route::get('/tambah_pengguna', array('as' => 'users.create', 'uses' => 'PenggunaController@create'));
Route::get('/edit_pengguna/{id}', array('as' => 'users.edit', 'uses' => 'PenggunaController@edit'));
Route::get('/view_pengguna/{id}', array('as' => 'users.show', 'uses' => 'PenggunaController@show'));
Route::post('/simpan_pengguna', array('as' => 'users.store', 'uses' => 'PenggunaController@store'));
Route::put('/update_pengguna/{id}', array('as' => 'users.update', 'uses' => 'PenggunaController@update'));
Route::delete('/hapus_pengguna/{id}', array('as' => 'users.destroy', 'uses' => 'PenggunaController@destroy'));
//sinkronisasi start//
Route::get('/sinkronisasi/ambil-data', 'SinkronisasiController@index')->name('ambil_data');
Route::get('/sinkronisasi/sekolah', 'SinkronisasiController@sekolah')->name('sekolah');
Route::get('/sinkronisasi/guru', 'SinkronisasiController@guru')->name('sinkronisasi_guru');;
Route::get('/sinkronisasi/rombongan-belajar', 'SinkronisasiController@rombongan_belajar')->name('sinkronisasi_rombel');
Route::get('/sinkronisasi/siswa-aktif', 'SinkronisasiController@siswa_aktif')->name('sinkronsasi_siswa_aktif');
Route::get('/sinkronisasi/siswa-keluar', 'SinkronisasiController@siswa_keluar')->name('sinkronsasi_siswa_keluar');
Route::get('/sinkronisasi/pembelajaran', 'SinkronisasiController@pembelajaran')->name('sinkronisasi_pembelajaran');
Route::get('/sinkronisasi/ekskul', 'SinkronisasiController@ekskul')->name('sinkronisasi_ekskul');
Route::get('/sinkronisasi/anggota-ekskul', 'SinkronisasiController@anggota_ekskul')->name('sinkronisasi_anggota_ekskul');
Route::get('/sinkronisasi/dudi', 'SinkronisasiController@dudi')->name('sinkronisasi_dudi');
Route::get('/sinkronisasi/jurusan', 'SinkronisasiController@jurusan')->name('sinkronisasi_jurusan');
Route::get('/sinkronisasi/kurikulum', 'SinkronisasiController@kurikulum')->name('sinkronisasi_kurikulum');
Route::get('/sinkronisasi/mata-pelajaran', 'SinkronisasiController@mata_pelajaran')->name('sinkronisasi_mata_pelajaran');
Route::get('/sinkronisasi/mapel-kur', 'SinkronisasiController@mapel_kur')->name('sinkronisasi_mapel_kur');
Route::get('/sinkronisasi/ref-kd', 'SinkronisasiController@ref_kd')->name('sinkronisasi_ref_kd');
Route::get('/sinkronisasi/proses-kd/{file}', array('as' => 'sinkronisasi.proses_kd', 'uses' => 'SinkronisasiController@proses_kd'));
//Route::get('/sinkronisasi/proses-artisan', 'SinkronisasiController@proses_artisan')->name('proses_artisan');
Route::get('/sinkronisasi/jumlah_kd', 'SinkronisasiController@jumlah_kd');
Route::get('/sinkronisasi/kirim-data', 'SinkronisasiController@kirim_data')->name('kirim_data');
Route::get('/sinkronisasi/proses-sync', 'SinkronisasiController@proses_sync')->name('proses_sync');
Route::get('/sinkronisasi/kirim-nilai', 'SinkronisasiController@kirim_nilai')->name('kirim_nilai');
Route::get('/sinkronisasi/anggota-by-rombel/{rombongan_belajar_id}', array('as' => 'sinkronisasi.anggota_by_rombel', 'uses' => 'SinkronisasiController@anggota_by_rombel'));
Route::get('/sinkronisasi/proses-artisan/{server}/{data}/{aksi}', array('as' => 'sinkronisasi.proses_artisan_sync', 'uses' => 'SinkronisasiController@proses_artisan_sync'));
Route::get('/sinkronisasi/hitung-data/{data}', array('as' => 'sinkronisasi.hitung_data', 'uses' => 'SinkronisasiController@hitung_data'));
//Route::get('/sinkronisasi/hitung-data/{table}/{query}', array('as' => 'sinkronisasi.hitung_data', 'uses' => 'SinkronisasiController@hitung_data'));
Route::get('/sinkronisasi/debug', 'SinkronisasiController@debug')->name('debug');
//sinkronisasi end//
Route::get('/guru', 'GuruController@index')->name('list_guru');
Route::get('/tendik', 'GuruController@tendik')->name('tendik');
Route::get('/instruktur', 'GuruController@instruktur')->name('instruktur');
Route::get('/asesor', 'GuruController@asesor')->name('asesor');
Route::get('/guru/list-guru/{query}', array('as' => 'guru.list_guru', 'uses' => 'GuruController@list_guru'));
Route::get('/guru/view/{guru_id}', array('as' => 'guru.view', 'uses' => 'GuruController@view'));
Route::get('/guru/edit-gelar/{guru_id}', array('as' => 'guru.edit_gelar', 'uses' => 'GuruController@edit_gelar'));
Route::get('/rombel', 'RombelController@index')->name('rombel');
Route::get('/rombel/list-rombel', 'RombelController@list_rombel')->name('list_rombel');
Route::get('/rombel/anggota/{rombel_id}', array('as' => 'rombel.anggota', 'uses' => 'RombelController@anggota'));
Route::get('/rombel/pembelajaran/{rombel_id}', array('as' => 'rombel.pembelajaran', 'uses' => 'RombelController@pembelajaran'));
Route::get('/rombel/keluarkan/{id}', array('as' => 'rombel.keluarkan', 'uses' => 'RombelController@keluarkan'));
Route::get('/rombel/pengajar', 'RombelController@pengajar')->name('get_pengajar');
Route::get('/rombel/kelompok/{id}', array('as' => 'rombel.kelompok', 'uses' => 'RombelController@kelompok'));
Route::post('/rombel/tambah_alias', array('as' => 'rombel.tambah_alias', 'uses' => 'RombelController@tambah_alias'));
Route::post('/rombel/simpan_pembelajaran', array('as' => 'rombel.simpan_pembelajaran', 'uses' => 'RombelController@simpan_pembelajaran'));
Route::get('/pd-aktif', 'SiswaController@index')->name('pd_aktif');
Route::get('/pd-keluar', 'SiswaController@keluar')->name('pd_keluar');
Route::get('/pd/list/{status}', array('as' => 'rombel.pembelajaran', 'uses' => 'SiswaController@list_siswa'));
Route::get('/pd/view/{siswa_id}', array('as' => 'siswa.view', 'uses' => 'SiswaController@view'));
Route::get('/konfigurasi', 'ConfigController@index')->name('konfigurasi');
Route::post('konfigurasi/simpan', 'ConfigController@simpan');
Route::get('/changelog', 'ChangelogController@index')->name('changelog');
Route::get('/check-update', 'UpdateController@index')->name('check_update');
Route::get('/proses-update', 'UpdateController@proses_update')->name('proses_update');
Route::get('/ekstrak', 'UpdateController@extract_to')->name('extract_to');
Route::get('/update-versi', 'UpdateController@update_versi')->name('update_versi');
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/referensi/mata-pelajaran', 'ReferensiController@index')->name('mata_pelajaran');
Route::get('/referensi/list-mata-pelajaran', 'ReferensiController@list_mata_pelajaran')->name('list_mata_pelajaran');
Route::get('/referensi/ekskul', 'ReferensiController@ekskul')->name('ekskul');
Route::get('/referensi/list-ekskul', 'ReferensiController@list_ekskul')->name('list_ekskul');
Route::get('/referensi/metode', 'ReferensiController@metode')->name('metode');
Route::get('/referensi/list-metode', 'ReferensiController@list_metode')->name('list_metode');
Route::get('/referensi/sikap', 'ReferensiController@sikap')->name('sikap');
Route::get('/referensi/kd', 'ReferensiController@kd')->name('ref_kompetensi_dasar');
Route::get('/referensi/list-kd', 'ReferensiController@list_kd')->name('list_kd');
Route::get('/referensi/ukk', 'ReferensiController@ukk')->name('ref_ukk');
Route::get('/referensi/list-ukk', 'ReferensiController@list_ukk')->name('list_ukk');
//Perencanaan Start//
Route::get('/perencanaan/rasio', 'PerencanaanController@index')->name('rasio');
Route::post('/perencanaan/simpan-rasio', array('as' => 'simpan_rasio', 'uses' => 'PerencanaanController@simpan_rasio'));
Route::get('/perencanaan/pengetahuan', 'PerencanaanController@pengetahuan')->name('perencanaan_pengetahuan');
Route::get('/perencanaan/tambah-pengetahuan', 'PerencanaanController@tambah_pengetahuan');
Route::get('/perencanaan/keterampilan', 'PerencanaanController@keterampilan')->name('perencanaan_keterampilan');
Route::get('/perencanaan/tambah-keterampilan', 'PerencanaanController@tambah_keterampilan');
Route::post('/perencanaan/simpan-perencanaan', array('as' => 'simpan_perencanaan', 'uses' => 'PerencanaanController@simpan_perencanaan'));
Route::get('/perencanaan/list-rencana/{kompetensi_id}', array('as' => 'perencanaan.list_rencana', 'uses' => 'PerencanaanController@list_rencana'));
Route::get('/perencanaan/bobot', 'PerencanaanController@bobot')->name('list_bobot');
Route::post('/perencanaan/simpan-bobot', array('as' => 'simpan_bobot', 'uses' => 'PerencanaanController@simpan_bobot'));
Route::get('/perencanaan/ukk', 'PerencanaanController@ukk')->name('perencanaan_ukk');
Route::get('/perencanaan/tambah-ukk', 'PerencanaanController@tambah_ukk');
Route::post('/perencanaan/simpan-ukk', array('as' => 'simpan_ukk', 'uses' => 'PerencanaanController@simpan_ukk'));
Route::get('/kunci-nilai/{rombongan_belajar_id}/{status}', array('as' => 'kunci_nilai', 'uses' => 'HomeController@kunci_nilai'));
Route::get('/generate-nilai/{pembelajaran_id}/{kompetensi_id}', array('as' => 'generate_nilai', 'uses' => 'HomeController@generate_nilai'));
//Perencanaan End//
//Penilaian Start//
Route::get('/penilaian/list-sikap', array('as' => 'penilaian.list_sikap', 'uses' => 'PenilaianController@list_sikap'));
Route::get('/penilaian/get-list-sikap', array('as' => 'penilaian.get_list_sikap', 'uses' => 'PenilaianController@get_list_sikap'));
Route::get('/penilaian/{kompetensi_id}', array('as' => 'penilaian.form_penilaian', 'uses' => 'PenilaianController@index'));
Route::post('/penilaian/simpan-nilai', 'PenilaianController@simpan_nilai')->name('penilaian.simpan_nilai');
Route::post('/penilaian/import_excel', 'PenilaianController@import_excel');
//Penilaian End//
//Query Ajax Start//
Route::post('/ajax/get-rombel', array('as' => 'ajax.get_rombel', 'uses' => 'AjaxController@get_rombel'));
Route::post('/ajax/get-mapel', array('as' => 'ajax.get_mapel', 'uses' => 'AjaxController@get_mapel'));
Route::post('/ajax/get-teknik', array('as' => 'ajax.get_teknik', 'uses' => 'AjaxController@get_teknik'));
Route::post('/ajax/get-kd', array('as' => 'ajax.get_kd', 'uses' => 'AjaxController@get_kd'));
Route::post('/ajax/get-siswa', array('as' => 'ajax.get_siswa', 'uses' => 'AjaxController@get_siswa'));
Route::post('/ajax/get-rencana', array('as' => 'ajax.get_rencana', 'uses' => 'AjaxController@get_rencana'));
Route::post('/ajax/get-kd-nilai', array('as' => 'ajax.get_kd_nilai', 'uses' => 'AjaxController@get_kd_nilai'));
Route::post('/ajax/get-rerata', array('as' => 'ajax.get_rerata', 'uses' => 'AjaxController@get_rerata'));
Route::post('/ajax/get-kompetensi', array('as' => 'ajax.get_kompetensi', 'uses' => 'AjaxController@get_kompetensi'));
Route::post('/ajax/get-remedial', array('as' => 'ajax.get_remedial', 'uses' => 'AjaxController@get_remedial'));
Route::post('/ajax/get-sikap', array('as' => 'ajax.get_sikap', 'uses' => 'AjaxController@get_sikap'));
Route::get('/ajax/get-bobot/{pembelajaran_id}/{metode_id}', array('as' => 'ajax.get_bobot', 'uses' => 'AjaxController@get_bobot'));
Route::post('/ajax/get-rekap-nilai', array('as' => 'ajax.get_rekap_nilai', 'uses' => 'AjaxController@get_rekap_nilai'));
Route::post('/ajax/get-analisis-nilai', array('as' => 'ajax.get_analisis_nilai', 'uses' => 'AjaxController@get_analisis_nilai'));
//Query Ajax End//
Route::get('/penilaian/exportToExcel/{rencana_penilaian_id}', 'PenilaianController@exportToExcel');
//Monitoring Start//
Route::get('/monitoring/rekap-nilai/', 'MonitoringController@index');
Route::get('/monitoring/unduh-nilai/{pembelajaran_id}', 'MonitoringController@unduh_nilai');
Route::get('/monitoring/analisis-nilai/', 'MonitoringController@analisis_nilai');
Route::get('/monitoring/analisis-remedial/', 'MonitoringController@analisis_remedial');
Route::get('/monitoring/capaian-kompetensi/', 'MonitoringController@capaian_kompetensi');
Route::get('/monitoring/prestasi-individu/', 'MonitoringController@prestasi_individu');
//Monitoring End//